--SQL Day 02

--case
select cast(10 as decimal(18,4))
select cast('10' as int)
select cast(10.65 as int)
select cast('2023-03-16' as datetime)
select GETDATE(), GETUTCDATE()
select day(getdate()),month(getdate()), year(getdate())

--convert
select convert(decimal(18,4), 10)
select convert(int, '10')
select convert (int, 10.65)
select convert(datetime, '2023-03-16')

--dateadd
select DATEADD(year, 2, getdate()), DATEADD(month, 3, getdate()), DATEADD(day, 5, getdate())

--datediff
select DATEDIFF(day, '2023-03-16', '2023-03-25'), DATEDIFF(month, '2023-03-16', '2024-06-16')
select DATEDIFF(year, '2023-03-16','2030-03-16')

--sub query
select name, address, email, panjang from mahasiswa
where panjang = (select max(panjang) from mahasiswa)

select into [dbo], [mahasiswa_new]
select name, address, email, panjang from mahasiswa

--create view

create view vwMahasiswa2
as
select * from mahasiswa

select * from vwMahasiswa2

--create index
create index index_name
on mahasiswa(name)

create index index_address_email 
on mahasiswa (address, email)

--create unique index (sama seperti index, namun kolom bersifat unik)

create unique index uniqueindex_address
on mahasiswa(address)

--drop index
drop index index_address_email on mahasiswa

--create unique index
create unique index uniqueindex_address
on mahasiswa(address)

--drop unique index
drop index uniqueindex_address on mahasiswa

---------------------------------------------------------------------------------

--add primary key
alter table mahasiswa
add constraint pk_name primary key(name)

--drop primary key
alter table mahasiswa drop constraint pk_adress

alter table mahasiswa
add constraint pk_id_address primary key (id, address)

--add primary key
alter table mahasiswa
add constraint pk_adress primary key(address)

--add unique constraint
alter table mahasiswa 
add constraint unique_address unique(address)

-- drop unique
alter table mahasiswa
drop constraint unique_adress

alter table mahasiswa
add constraint unique_panjang unique(panjang)

update mahasiswa set panjang=null where id=5