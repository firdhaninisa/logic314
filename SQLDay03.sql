--Tugas 03

create database DB_Sales

create table SALESPERSON(
ID int primary key identity(1,1),
NAME varchar(50) not null,
BOD date not null,
SALARY decimal (18,2) not null
)

insert into SALESPERSON (NAME, BOD, SALARY) values
('Abe', '1988/11/9', 140000),
('Bob', '1978/11/9', 44000),
('Christ', '1983/11/9', 40000),
('Dan', '1980/11/9', 52000),
('Ken', '1977/11/9', 115000),
('Joe', '1990/11/9', 38000)

create table ORDERS(
ID int primary key identity(1,1),
ORDER_DATE date not null,
CUST_ID int null,
SALESPERSON_ID int not null,
AMOUNT decimal (18,2) not null
)

insert into ORDERS (ORDER_DATE, CUST_ID, SALESPERSON_ID, AMOUNT) values
('2020-08-02', 4, 2, 540),
('2021-01-22', 4, 5, 1800),
('2019-07-14', 9, 1, 460),
('2018-01-29', 7, 2, 2400),
('2021-02-03', 6, 4, 600),
('2020-03-02', 6, 4, 720),
('2021-05-06', 9, 4, 150)

--jawaban

--a. nama sales yang memiliki order lebih dari satu

select * from SALESPERSON
select * from ORDERS

select SAL.NAME, COUNT(ORD.ID) [JUMLAH_ORDER]
from SALESPERSON as SAL
JOIN ORDERS as ORD
ON SAL.ID = ORD.SALESPERSON_ID
group by SAL.NAME
having COUNT(ORD.ID) > 1

--b. informasi nama sales yang total amount ordernya di atas 1000
select SAL.NAME, SUM(ORD.AMOUNT)[TOTAL_AMOUNT]
from SALESPERSON as SAL
JOIN ORDERS as ORD
ON SAL.ID = ORD.SALESPERSON_ID
group by SAL.NAME
having sum(ORD.AMOUNT) > 1000

--c. tampilkal nama sales, umur, gaji dan total amount order yang tahun order >= 2020 dan urutkan umur
select SAL.NAME,
DATEDIFF(YEAR,SAL.BOD, GETDATE())[UMUR], 
SAL.SALARY, 
sum(ORD.AMOUNT)[TOTAL_AMOUNT]
from SALESPERSON as SAL
JOIN ORDERS as ORD
ON SAL.ID = ORD.SALESPERSON_ID
WHERE YEAR(ORD.ORDER_DATE) >= 2020
GROUP BY SAL.NAME, SAL.SALARY, SAL.BOD
ORDER BY UMUR ASC

--d. cari rata-rata total amount dari masing-masing sales urutkan yang paling besar
select SAL.NAME, AVG(ORD.AMOUNT)[RATARATA_TOTALAMOUNT]
from SALESPERSON as SAL
JOIN ORDERS as ORD
ON SAL.ID = ORD.SALESPERSON_ID
group by SAL.NAME
order by RATARATA_TOTALAMOUNT desc

--e. pemberian bonus bagi sales yang memiliki order > 2 dan total order > 1000 sebanyak 30% salary

SELECT SAL.NAME, COUNT(ORD.ID)[TOTAL_ORDER], 
SUM(ORD.AMOUNT)[TOTAL_AMOUNT], SAL.SALARY*0.3 [BONUS]
FROM SALESPERSON AS SAL
JOIN ORDERS AS ORD
ON SAL.ID = ORD.SALESPERSON_ID
GROUP BY SAL.NAME, SAL.SALARY
HAVING COUNT(ORD.ID) > 2 AND SUM(ORD.AMOUNT) > 1000

--f. sales yang belum memiliki order sama sekali
SELECT SAL.NAME
FROM SALESPERSON AS SAL
LEFT JOIN ORDERS AS ORD
ON SAL.ID = ORD.SALESPERSON_ID
WHERE ORD.SALESPERSON_ID IS NULL

--g. gaji sales akan dipotong jika tidak memiliki order
select sal.NAME, sal.SALARY * 0.02 [POTONGAN],
sal.SALARY[GAJI],
sal.SALARY - (sal.SALARY * 0.02) [GAJI_BERSIH]
from SALESPERSON as sal
left join ORDERS as ord
on sal.ID = ord.SALESPERSON_ID
where ord.SALESPERSON_ID is null


