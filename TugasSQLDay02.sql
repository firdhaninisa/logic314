--Tugas SQL Day 02

create database DB_Entertainer

drop table artis

create table artis(
kd_artis varchar(100) primary key not null,
nm_artis varchar(100) not null,
jk varchar(100) not null,
bayaran bigint not null,
award int,
negara varchar(100) not null
)


select * from artis

--alter table artis
--add constraint pk_kd_artis primary key(kd_artis)

insert into artis (kd_artis, nm_artis, jk, bayaran, award, negara) values
('A001', 'ROBERT DOWNEY JR', 'PRIA', 3000000000, 2, 'AS'),
('A002', 'ANGELINA JOLIE', 'WANITA', 700000000, 1, 'AS'),
('A003', 'JACKIE CHAN', 'PRIA', 200000000, 7, 'HK'),
('A004', 'JOE TASLIM', 'PRIA', 350000000, 1, 'ID'),
('A005', 'CHELSEA ISLAN', 'WANITA', 300000000, 0, 'ID')

create table film(
kd_film varchar(10) not null,
nm_film varchar(55) not null,
genre varchar(55) not null,
artis varchar(55) not null,
produser varchar(55) not null,
pendapatan int not null,
nominasi int
)

alter table film alter column pendapatan bigint

insert into film (kd_film, nm_film, genre, artis, produser, pendapatan, nominasi) values
('F001', 'IRON MAN', 'G001', 'A001', 'PD01', 2000000000, 3),
('F002', 'IRON MAN 2', 'G001', 'A001', 'PD01', 1800000000, 2),
('F003', 'IRON MAN 3', 'G001', 'A001', 'PD01', 1200000000, 0),
('F004', 'AVENGER : CIVIL WAR', 'G001', 'A001', 'PD01', 2000000000, 1),
('F005', 'SPIDERMAN HOME COMING', 'G001', 'A001', 'PD01', 1300000000, 0),
('F006', 'THE RAID', 'G001', 'A004', 'PD03', 800000000, 5),
('F007', 'FAST & FURIOUS', 'G001', 'A004', 'PD05', 830000000, 2),
('F008', 'HABIBIE DAN AINUM', 'G004', 'A005', 'PD03', 670000000, 4),
('F009', 'POLICE STORY', 'G001', 'A003', 'PD02', 700000000, 3),
('F010', 'POLICE STORY 2', 'G001', 'A003', 'PD02', 710000000, 1)

insert into film (kd_film, nm_film, genre, artis, produser, pendapatan, nominasi) values
('F011', 'POLICE STORY 3', 'G001', 'A003', 'PD02', 615000000, 0),
('F012', 'RUSH HOUR', 'G003', 'A003', 'PD05', 695000000, 2),
('F013', 'KUNGFU PANDA', 'G003', 'A003', 'PD02', 923000000, 5)



select * from film

alter table film
add constraint pk_kd_film primary key(kd_film)

create table produser(
kd_produser varchar(50) not null,
nm_produser varchar(50) not null,
international varchar(50) not null
)

insert into produser (kd_produser, nm_produser, international) values
('PD01', 'MARVEL', 'YA'),
('PD02', 'HONGKONG CINEMA', 'YA'),
('PD03', 'RAPI FILM', 'TIDAK'),
('PD04', 'PARKIT', 'TIDAK'),
('PD05', 'PARAMOUNT PICTURES', 'YA')

alter table produser
add constraint pk_kd_produser primary key(kd_produser)

create table negara(
kd_negara varchar(100) primary key not null,
nm_negara varchar(100) not null
)

insert into negara (kd_negara, nm_negara) values
('AS', 'AMERIKA SERIKAT'),
('HK', 'HONGKONG'),
('ID', 'INDONESIA'),
('IN', 'INDIA')

create table genre(
kd_genre varchar(50) primary key not null,
nm_genre varchar(50) not null
)

insert into genre (kd_genre, nm_genre) values
('G001', 'ACTION'),
('G002', 'HORROR'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER'),
('G006', 'FICTION')

--1. Menampilkan jumlah pendapatan produser marvel secara keseluruhan

select produser.nm_produser, sum(film.pendapatan)
from film
JOIN produser
ON produser.kd_produser = film.produser
WHERE nm_produser='MARVEL' GROUP BY nm_produser

select * from produser
select * from film

--2. Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi	

select nm_film, nominasi from film where nominasi = 0

--3. Menampilkan nama film yang huruf depannya 'p'

select nm_film from film where nm_film like 'p%'

--4. Menampilkan nama film yang huruf terakhir 'y'

select nm_film from film where nm_film like '%y'

--5. Menampilkan nama film yang mengandung huruf 'd'

select nm_film from film where nm_film like '%d%'

--6. Menampilkan nama film dan artis

select * from artis
select * from film

select film.nm_film, artis.nm_artis
from film
JOIN artis
ON film.artis = kd_artis

--7. Menampilkan nama film yang artisnya berasal dari negara hongkong

select * from artis
select * from film
select * from negara

select film.nm_film, negara.kd_negara as negara
from artis
JOIN negara
ON artis.negara = negara.kd_negara
join film
ON artis.kd_artis = film.artis
WHERE
artis.negara = 'HK'

/*select film.nm_film, artis.negara
from film
join artis
on film.kd_film = artis.negara
where negara = 'HK'*/

--8. Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o'

select * from artis
select * from film
select * from negara

select film.nm_film, negara.nm_negara
from artis
JOIN negara
ON artis.negara = negara.kd_negara
join film
ON artis.kd_artis = film.artis
WHERE negara.nm_negara NOT LIKE '%o%'


--9. Menampilkan nama artis yang tidak pernah bermain film (belum)

select * from artis
select * from film

select artis.nm_artis
from artis
LEFT JOIN film
ON artis.kd_artis = film.artis
WHERE film.nm_film is null

select artis.nm_artis, film.nm_film
from artis
LEFT JOIN film
ON artis.kd_artis = film.artis
WHERE film.nm_film is null

--10. Menampilkan nama artis yang bermain film dengan genre drama

select * from artis
select * from film
select * from genre

select artis.nm_artis, genre.nm_genre
from artis
join film
ON artis.kd_artis = film.artis
join genre
ON genre.kd_genre = film.genre
WHERE
nm_genre = 'DRAMA'

select artis.nm_artis, genre.nm_genre
from artis
join film
ON artis.kd_artis = film.artis
join genre
ON genre.kd_genre = film.genre
WHERE
nm_genre like '%n'

--11. Menampilkan nama artis yang bermain film dengan genre Action
-- distict mencegah perulangan, redudant, atau duplicate data

select distinct artis.nm_artis, genre.nm_genre
from artis
join film
ON artis.kd_artis = film.artis
join genre
ON genre.kd_genre = film.genre
WHERE
nm_genre = 'ACTION'

select artis.nm_artis, genre.nm_genre
from artis
join film
ON artis.kd_artis = film.artis
join genre
ON genre.kd_genre = film.genre
WHERE
nm_genre = 'ACTION'
group by artis.nm_artis, genre.nm_genre

--12. Menampilkan data negara dengan jumlah filmnya	

select * from negara
select * from film
select * from produser
select * from artis

select negara.kd_negara, negara.nm_negara, count(film.artis) as jumlah_Film
from negara
left join artis
on negara.kd_negara = artis.negara
left join film
on artis.kd_artis = film.artis
group by negara.kd_negara, negara.nm_negara
order by negara.nm_negara asc

--13. Menampilkan nama film yang skala internasional

select * from film
select * from produser

select film.nm_film
from film
join produser
on film.produser = produser.kd_produser
where international='YA'

--14. Menampilkan jumlah film dari masing2 produser	

select * from film
select * from produser

select produser.nm_produser, count(film.produser) as jumlah_film
from film
right join produser
on film.produser = produser.kd_produser
group by produser.nm_produser

select * from produser
select * from film
