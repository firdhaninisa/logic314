﻿using System;

namespace Logic01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Konversi();
            //operatorAritmatika();
            //modulus();
            //operatorPenugasan();
            //operatorPerbandingan();
            //operatorLogika();
            //Penjumlahan();

            Console.ReadKey();

        }

        static void Penjumlahan()
        {
            int mangga, apel;

            Console.WriteLine("--Method ReturnType--");
            Console.Write("Jumlah mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("Jumlah apel = ");
            apel = int.Parse(Console.ReadLine());

            int hasil = HitungJumlah(mangga, apel);

            Console.WriteLine($"Hasil mangga + apel = {hasil}");
        }

        static int HitungJumlah (int mangga, int apel)
        {
            int hasil = 0;
            hasil = mangga + apel;
            return hasil;
        }

        static void operatorLogika()
        {
            Console.Write("Masukkan umur : ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("Masukkan password : ");
            string password = Console.ReadLine();

            bool isAdult = umur > 18; //pernyataan 1
            bool isPasswordValid = password == "admin"; //pernyataan 2

            if(isAdult && isPasswordValid)
            {
                Console.WriteLine("Anda sudah dewasa dan password valid");
            }
            else if (isAdult && !isPasswordValid)
            {
                Console.WriteLine("Anda sudah dewasa dan password invalid");
            }
            else if (!isAdult && isPasswordValid)
            {
                Console.WriteLine("Anda belum dewasa dan password valid");
            }
            else
            {
                Console.WriteLine("Anda belum dewasa dan password tidak valid");
            }
        }

        static void operatorPerbandingan()
        {
            int mangga, apel = 0;

            Console.WriteLine("--Operator Perbandingan--");
            
            Console.Write("Jumlah mangga = ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah apel = ");
            apel = int.Parse(Console.ReadLine());

            Console.WriteLine("Hasil Perbandingan : ");
            Console.WriteLine($"mangga > apel = {mangga > apel}");
            Console.WriteLine($"mangga >= apel = {mangga >= apel}");
            Console.WriteLine($"mangga < apel = {mangga < apel}");
            Console.WriteLine($"mangga <= apel = {mangga <= apel}");
            Console.WriteLine($"mangga != apel = {mangga != apel}");
            Console.WriteLine($"mangga == apel = {mangga == apel}");
            
            
            
            //Console.WriteLine($"mangga %= apel = {mangga %= apel}");

        }

        static void operatorPenugasan()
        {
            int mangga = 10;
            int apel = 8;

            // diisi ulang
            mangga = 15;

            Console.WriteLine("--Operator Penugasan--");
            Console.WriteLine($"mangga = {mangga}");
            Console.WriteLine($"apel = {apel}");

            //apel = 15;

            //Console.WriteLine($"apel = " {apel});

            apel += mangga;
            Console.WriteLine($"apel += {apel}");

            mangga = 15;
            apel = 8;
            apel -= mangga;
            Console.WriteLine($"apel -= {apel}");

            mangga = 15;
            apel = 8;
            apel *= mangga;
            Console.WriteLine($"apel *= {apel}");

            mangga = 15;
            apel = 8;
            apel /= mangga;
            Console.WriteLine($"apel /= {apel}");

            mangga = 15;
            apel = 8;
            apel %= mangga;
            Console.WriteLine($"apel %= {apel}");

        }

        static void operatorAritmatika()
        {
            int mangga, apel, hasil = 0;

            Console.WriteLine("--Opeerator Aritmatika--");
            Console.Write("Jumlah mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("Jumlah apel = ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga + apel;

            Console.WriteLine($"Hasil mangga + apel = {hasil}");
        }

        static void modulus()
        {
            int mangga, apel, hasil = 0;

            Console.WriteLine("--Modulus--");
            Console.Write("Jumlah mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("Jumlah apel = ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga % apel;

            Console.WriteLine($"Hasil magga % apel= {hasil}");
        }

        static void Konversi()
        {
            int umur = 19;
            string strUmur = umur.ToString();

            int myInt = 10;
            double myDouble = 5.25;
            bool myBool = false;

            string myString = Convert.ToString(myInt);
            double myConvDouble = Convert.ToDouble(myInt);
            int myConvInt = Convert.ToInt32(myDouble);
            string myConvString = Convert.ToString(myBool);

            Console.WriteLine("Convert int to string: " + myString);
            Console.WriteLine("Convert int to double: " + myConvDouble);
            Console.WriteLine("Convert double to int: " + myConvInt);
            Console.WriteLine("Convert bool to string: " + myConvString);

            Console.WriteLine(strUmur);

            
        }
    }
}
