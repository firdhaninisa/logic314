﻿using System;
using System.Collections.Generic;

namespace Logic06
{
    public class Program
    {
        static void Main(string[] args)
        {
            listRemove();
            //listAdd();
            //contohClass();
            //listClass();

            Console.ReadKey();
        }

        static void listRemove()
        {
            List<User> listUser = new List<User>()
            {
                new User(){Name = "Isni Dwitiniardi", Age = 22},
                new User(){Name = "Astika", Age = 23},
                new User(){Name = "Alfi Azizi", Age = 23}
            };

            listUser.Remove(new User() { Name = "Anwar", Age = 24 });

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age);
            }
        }

        static void listAdd()
        {
            List<User> listUser = new List<User>()
            {
                new User(){Name = "Isni Dwitiniardi", Age = 22},
                new User(){Name = "Astika", Age = 23},
                new User(){Name = "Alfi Azizi", Age = 23}
            };

            listUser.Add(new User() { Name = "Anwar", Age = 24 });

            for (int i = 0; i < listUser.Count; i++)
                Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age);
        }

        static void listClass()
        {
            List<User> listUser = new List<User>()
            {
                new User(){Name = "Isni Dwitiniardi", Age = 22},
                new User(){Name = "Astika", Age = 23},
                new User(){Name = "Alfi Azizi", Age = 23}
            };

            for (int i = 0; i < listUser.Count; i++)
                Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age);
        }

        static void contohClass()
        {
            //list();
            //Mobil mobil = new Mobil();
            Mobil mobil = new Mobil("RI SATU");

            mobil.percepat();
            mobil.maju();
            mobil.isiBensin(12);

            string platno = mobil.getPlatNo();

            Console.WriteLine($"Plat Nomor : {platno}");
            Console.WriteLine($"Bensin : {mobil.bensin}");
            Console.WriteLine($"Kecepatan : {mobil.kecepatan}");
            Console.WriteLine($"Posisi : {mobil.posisi}");
        }

        static void list()
        {
            Console.WriteLine("-----List-----");
            List<string> list = new List<string>()
            {
                "Astika",       //index 1
                "Marchelino",   //index 2
                "Alwi Fadli",   //index 3
                "Toni"          //index 4
            };

            Console.WriteLine(String.Join(", ", list));
        }
    }
}
