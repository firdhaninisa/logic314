﻿using System;

namespace Logic05
{
    class Program
    {
        static void Main(string[] args)
        {
            //contain();
            //padLeft();
            convertArrayAll();

            Console.ReadKey();
        }

        static void convertArrayAll()
        {
            Console.WriteLine("--Convert Array All--");
            Console.Write("Masukkan angka array (pakai koma) : ");
            int[] array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.WriteLine(String.Join("\t", array));
        }

        static void padLeft()
        {
            Console.WriteLine("--Pad Left--");
            Console.Write("Masukkan kalimat : ");
            int input = int.Parse(Console.ReadLine());
            Console.Write("Masukkan panjang karakter : ");
            int panjang = int.Parse(Console.ReadLine());
            Console.WriteLine("Masukkan Char : ");
            char chars = char.Parse(Console.ReadLine());


            Console.WriteLine($"Hasil PadLeft : {input.ToString().PadLeft(panjang, chars)}");
        }

        
        static void contain()
        {
            Console.WriteLine("--Contain--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Masukkan contain : ");
            string contain = Console.ReadLine();

            if (kalimat.Contains(contain))
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini mengandung {contain}");
            }
            else
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini tidak mengandung {contain}");
            }
        }
    }
}
