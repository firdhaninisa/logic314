﻿using System; 
using System.Collections.Generic;

namespace Logic_10
{
    class Program
    {
        static void Main(string[] args)
        {
            //HACKERRANK
            //SimpleArraySum();
            //BilanganPrima();
            //Satu();
            //Dua();
            //Tiga();
            //Tiga_2();
            //Empat();
            //Lima();
            //Enam();
            //Sembilan();
            

            Console.ReadKey();
        }

        static void Satu()
        {
            Console.Write("Input kalimat\t: ");
            string kalimat = Console.ReadLine();

            int hasil = 0;

            for(int i = 0; i < kalimat.Length; i++)
            {
                if (char.IsUpper(kalimat[i]))
                {
                    hasil++;
                }
            }

            Console.WriteLine($"Output : {hasil}");




        }

        static void Dua()
        {

            Console.WriteLine("----------Nomor Invoice Penjualan----------");

            Console.Write("Start\t: ");
            int start = int.Parse(Console.ReadLine());
            Console.Write("End\t: ");
            int end = int.Parse(Console.ReadLine());

            /*DateTime dt = new DateTime(2022, 08, 07);
            int tanggal = dt.Day;
            int bulan = dt.Month;
            int tahun = dt.Year;*/

            /*string dateString = "07/08/2022";
            DateTime dt1 = DateTime.Parse(dateString);
            int tanggal = dt1.Day;
            int bulan = dt1.Month;
            int tahun = dt1.Year;*/


            DateTime dt2 = DateTime.Now;

            int tanggal = dt2.Day;
            int bulan = dt2.Month;
            int tahun = dt2.Year;

            for (int i = start; i <= end; i++)
            {
                Console.WriteLine($"XA-{tanggal}{bulan}{tahun}-{i.ToString().PadLeft(5, '0')}");
            }
        }

        static void Tiga()
        {
            Console.WriteLine("----------Jumlah Buah yang Ada di Dapur----------");
            Console.Write("Keranjang 1\t: ");
            int ker_1 = int.Parse(Console.ReadLine());
            Console.Write("Keranjang 2\t: ");
            int ker_2 = int.Parse(Console.ReadLine());
            Console.Write("Keranjang 3\t: ");
            int ker_3 = int.Parse(Console.ReadLine());
            Console.Write("Keranjang yang dibawa ke pasar : ");
            int ker_psr = int.Parse(Console.ReadLine());

            int tampung = 0;

            switch (ker_psr)
            {
                case 1:
                    tampung = ker_2 + ker_3;
                    break;
                case 2:
                    tampung = ker_1 + ker_3;
                    break;
                case 3:
                    tampung = ker_1 + ker_2;
                    break;
                default:
                    Console.Write("Pilih antara 1 sampai 3: ");
                    break;
            }

            Console.WriteLine($"Sisa Buah : {tampung}");

        }

        static void Tiga_2()
        {
            Console.WriteLine("----------Jumlah Buah yang Ada di Dapur----------");


        ulang1:
            Console.Write("Keranjang 1\t: ");
            string ker_1 = Console.ReadLine();
            Console.Write("Keranjang 2\t: ");
            string ker_2 = Console.ReadLine();
            Console.Write("Keranjang 3\t: ");
            string ker_3 = Console.ReadLine();
        ulang:
            Console.Write("Keranjang yang dibawa ke pasar (1-3): ");
            string ker_psr = Console.ReadLine().ToLower();

            int tampung = 0;

            //int tampung_ker = 0;

            if (ker_1 == "kosong")
            {
                ker_1 = "0";
            }
            else if (ker_2 == "kosong")
            {
                ker_2 = "0";
            }
            else if (ker_3 == "kosong")
            {
                ker_3 = "0";
            }
            else
            {
                Console.WriteLine("Masukkan input yang sesuai (kosong/ angka)");
                goto ulang1;
            }

            switch (ker_psr)
            {
                case "1":
                    tampung = int.Parse(ker_2) + int.Parse(ker_3);
                    break;
                case "2":
                    tampung = int.Parse(ker_1) + int.Parse(ker_3);
                    break;
                case "3":
                    tampung = int.Parse(ker_1) + int.Parse(ker_2);
                    break;
                default:
                    Console.WriteLine("1 sampai 3 saja");
                    goto ulang;
            }

            Console.WriteLine($"Sisa Buah\t: {tampung}");

        }

        static void Empat()
        {
            Console.WriteLine("----------Donasi Baju----------");
            Console.Write("Input baju untuk: ");
            int input = int.Parse(Console.ReadLine());


        }

        static void Lima()
        {
            Console.WriteLine("----------Nilai Mahasiswa----------");

        }

        static void Enam()
        {
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            string huruf = "abcdefghijklmnopqrstuvwxyz";

            char[] huruf_array = huruf.ToCharArray();

            bool isPangram = true;
            for(int i = 0; i < huruf.Length; i++)
            {
                if(kalimat.Contains(huruf_array[i]) == false)
                {
                    isPangram = false;
                    break;
                }
            }
            if (isPangram)
            {
                Console.WriteLine("Pangram");
            }
            else
            {
                Console.WriteLine("not pangram");
            }
        }

        static void Tujuh()
        {

        }

        static void Delapan()
        {

        }

        static void Sembilan()
        {
            Console.WriteLine("----------Transaksi Beli Pulsa----------");
            Console.Write("Masukkan jumlah pulsa\t: ");
            int pulsa = int.Parse(Console.ReadLine());

            int tampung = 0;
            if (pulsa >= 0 && pulsa <= 10000)
            {
                tampung = 0;
            }
            else if (pulsa > 10000 && pulsa <= 30000)
            {
                tampung = (pulsa - 10000) / 1000;
            }
            else if (pulsa > 30000)
            {
                tampung = 20 + (((pulsa - 30000) / 1000) * 2);
            }
            else
            {
                Console.WriteLine($"Nominal pulsa tidak terdaftar");
            }
            Console.WriteLine($"Poin Pulsa\t\t: {tampung}");

        }

        static void BilanganPrima()
        {
            Console.WriteLine("----------Bilangan Prima----------");
            Console.Write("Masukkan jumlah bilangan : ");
            int bilangan = int.Parse(Console.ReadLine());

            bool prima = true;
            if (bilangan > 2)
            {

                //for (int i = 2; i < bilangan; i++)
                //{
                    for (int j = 2; j < bilangan; j++)
                    {
                        if (bilangan % j == 0)
                        {
                            prima = false;
                            break;
                        }
                    }
                    if (prima)
                    {
                    Console.Write($"{bilangan} adalah bilangan prima");
                    prima = true;
                    }
                    else
                    Console.Write($"Bilangan {bilangan} bukan bilangan prima");
                //}
            }
            else
            {
                Console.WriteLine("Tidak ada bilangan prima yang bisa dituliskan");
                Console.ReadLine();
            }
        }

        static void StrongPassword()
        {
            string number = "1234567890";            
        }

        static void SimpleArraySum()
        {

            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(10);
            list.Add(11);

            int hasil = simpleArraySum(list);

            Console.WriteLine(hasil);

        }

        static int simpleArraySum(List<int> ar)
        {
            int result = 0;

            for(int i = 0; i < ar.Count; i++)
            {
                result += ar[i];
            }
            return result;
        }

            
    }
}
