﻿using System;

namespace logic3_pr
{
    class Program
    {
        static void Main(string[] args)
        {
            //soalDelapan();
            //soalDelapan();
            //soalTujuh();
            //soalEnam();
            //soalLima();
            //soalEmpat();
            //soalTiga();
            //soalDua();
            //soalSatu();
            Console.ReadKey();
        }

        static void soalDelapan()
        {

            Console.WriteLine("=====Rata-rata Nilai=====");
            double nilaiMTK, nilaiFisika, nilaiKimia;
            Console.Write("Masukkan nilai MTK : ");
            nilaiMTK = double.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Fisika : ");
            nilaiFisika = double.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai Kimia : ");
            nilaiKimia = double.Parse(Console.ReadLine());

            double nilaiRata = (nilaiMTK + nilaiFisika + nilaiKimia) / 3;


            if (nilaiRata >= 75 && nilaiRata < 100)
            {
                Console.WriteLine($"Nilai Rata-Rata : {nilaiRata}");
                Console.WriteLine("Selamat Kamu Berhasil Kamu Hebat");
                Console.WriteLine("Selamat");
                Console.WriteLine("Kamu Hebat");

            }
            if (nilaiRata >= 0 && nilaiRata < 75)
            {
                Console.Write($"Nilai Rata-Rata : {nilaiRata}");
                Console.WriteLine("Maaf");
                Console.WriteLine("Kamu Gagal");
            }
            else
            {
                Console.WriteLine("Masukkan nilai yang sesuai");
            }
        }

        static void soalTujuh()
        {
            double berat, tinggi;

            Console.WriteLine("=====BMI=====");
            Console.Write("Masukkan berat badan anda (kg) : ");
            berat = double.Parse(Console.ReadLine());
            Console.Write("Masukkan tinggi badan anda (cm) : ");
            tinggi = double.Parse(Console.ReadLine());

            double bmi = (berat) / ((tinggi * tinggi) / 10000);

            if (bmi >= 18.5 && bmi < 25)
            {
                Console.WriteLine($"Nilai bmi Anda adalah : {bmi}");
                Console.WriteLine("Anda termasuk berbadan langsing");
            }

            else if (bmi >= 25)
            {
                Console.WriteLine($"Nilai bmi Anda adalah : {bmi}");
                Console.WriteLine("Anda termasuk berbadan gemuk");
            }
            else
            {
                Console.WriteLine("Masukkan nilai yang sesuai");
            }
        }

        static void soalEnam()
        {
            double tunjangan, gapok, pajak, banyakBulan;
            string namaSoalEnam;

            double bpjs = 0.03;

            Console.WriteLine("======Gaji Karyawan=====");
            Console.Write("Nama : ");
            namaSoalEnam = Console.ReadLine();
            Console.Write("Tunjangan : ");
            tunjangan = double.Parse(Console.ReadLine());
            Console.Write("Gapok : ");
            gapok = double.Parse(Console.ReadLine());
            Console.Write("Banyak Bulan : ");
            banyakBulan = double.Parse(Console.ReadLine());


            if (gapok + tunjangan <= 5000000)
            {
                pajak = 0.05;

            }
            else if (gapok + tunjangan >= 5000000 && gapok <= 10000000)
            {
                pajak = 0.1;
                
            }
            else if (gapok + tunjangan > 10000000)
            {
                pajak = 0.15;

            }
                else
                {
                pajak = 0;    
                }

            double totalPajak = pajak * (gapok + tunjangan);
            double totalBPJS = bpjs * (gapok + tunjangan);
            double gajiBulan = (gapok + tunjangan) - (pajak + bpjs);
            double totalGaji = ((gapok + tunjangan) - (pajak + bpjs)) * banyakBulan;

            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine($"Karyawan atas nama {namaSoalEnam} slip gaji sebagai berikut");
            Console.WriteLine($"Pajak : Rp.{totalPajak}");
            Console.WriteLine($"BPJS : Rp.{totalBPJS}");
            Console.WriteLine($"Gaji/Bulan : {gajiBulan}");
            Console.WriteLine($"Total Gaji/Banyak Bulan : {totalGaji}");
            }

        static void soalLima()
        {

            string nama;
            int tahun;
            Console.WriteLine("=====Istilah Generasi Berdasarkan Tahun Lahir=====");
            Console.Write("Masukkan nama Anda : ");
            nama = Console.ReadLine();
            Console.Write("Tahun berapa Anda lahir? ");
            tahun = int.Parse(Console.ReadLine());


            if (tahun >= 1944 && tahun <= 1964)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir Anda tergolong Baby Boomer");
            }
            else if (tahun >= 1965 && tahun <= 1979)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir Anda tergolong Generasi X");
            }
            else if (tahun <= 1980 && tahun <= 1994)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir Anda tergolong Generasi Y (Millenials");
            }
            else
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir Anda tergolong Generasi Z");
            }
        }

        static void soalEmpat()
        {
            //double diskonOngkir1, diskonOngkir2, diskonOngkir3, diskonOrder1, diskonOrder2, diskonOrder3;

            double order, voucherOrder, ongkirOrder;

            Console.WriteLine("=====Diskon Belanja dan Diskon Ongkir=====");
            Console.Write("Belanja : ");
            order = double.Parse(Console.ReadLine());
            Console.Write("Ongkos Kirim = ");
            ongkirOrder = double.Parse(Console.ReadLine());
            Console.Write("Pilih voucher : ");
            voucherOrder = double.Parse(Console.ReadLine());


            double diskonOngkir1 = 5000;
            double diskonOngkir2 = 10000;
            double diskonOngkir3 = 20000;

            double diskonOrder1 = 5000;
            double diskonOrder2 = 10000;
            double diskonOrder3 = 1000;

            if (order >= 30000 && order < 50000 && voucherOrder == 1)
            {
                Console.WriteLine($"Belanja : {order}");
                Console.WriteLine($"Ongkos Kirim : {ongkirOrder}");
                Console.WriteLine($"Diskon Ongkir : {diskonOngkir1}");
                Console.WriteLine($"Diskon Belanja : {diskonOrder1}");
                Console.WriteLine($"Total Belanja : {order + ongkirOrder - diskonOrder1 - diskonOngkir1}");

            }
            else if (order >= 50000 && order < 100000 && voucherOrder == 2)
            {
                Console.WriteLine($"Belanja : {order}");
                Console.WriteLine($"Ongkos Kirim : {ongkirOrder}");
                Console.WriteLine($"Diskon Ongkir : {diskonOngkir2}");
                Console.WriteLine($"Diskon Belanja : {diskonOrder2}");
                Console.WriteLine($"Total Belanja : {order + ongkirOrder - diskonOrder2 - diskonOngkir2}");
            }
            else if (order >= 100000 && voucherOrder == 3)
            {
                Console.WriteLine($"Belanja : {order}");
                Console.WriteLine($"Ongkos Kirim : {ongkirOrder}");
                Console.WriteLine($"Diskon Ongkir : {diskonOngkir3}");
                Console.WriteLine($"Diskon Belanja : {diskonOrder3}");
                Console.WriteLine($"Total Belanja : {order + ongkirOrder - diskonOrder3 - diskonOngkir3}");
            }
            else
            {
                Console.WriteLine("Tidak ada diskon");
            }
        }

        static void soalTiga()
        {

            string promo;
            double belanja, jarak;

            Console.WriteLine("=====Promo GrabFood=====");
            Console.Write("Belanja : ");
            belanja = double.Parse(Console.ReadLine());
            Console.Write("Jarak : ");
            jarak = double.Parse(Console.ReadLine());
            Console.Write("Masukan Promo = ");
            promo = Console.ReadLine().ToUpper();
            double maxDiskon = belanja * 0.4;
            double ongkir = ((jarak - 5) * 1000) + 5000; //per km
                                                            // 5 KM pertama 5000

            if (belanja >= 30000 && promo == "JKTOVO" && maxDiskon <= 30000)
            {
                Console.WriteLine($"Belanja : {belanja}");
                //Console.WriteLine($"Diskon : {maxDiskon * belanja});
                Console.WriteLine($"Diskon 40% : {maxDiskon}");
                Console.WriteLine($"Ongkir : {ongkir}");
                Console.WriteLine($"Total Belanja : {belanja + maxDiskon + ongkir}");
            }
            else
        {
            Console.WriteLine("Promo tidak terdaftar");
        }
        }

        static void soalDua()
        {

            Console.WriteLine("--Dapat Poin--");
            //Console.WriteLine("Jika membeli pulsa 10000 maka mendapatkan poin 80");
            //Console.WriteLine("Jika membeli pulsa 20000 maka mendapatkan poin 80");
            Console.Write("Masukkan jumlah pulsa : ");
            int pulsa = int.Parse(Console.ReadLine());
            int poin1 = 80;
            int poin2 = 200;
            int poin3 = 400;
            int poin4 = 800;


            if (pulsa >= 10000 && pulsa < 25000)
            {
                Console.WriteLine($"Pulsa : {pulsa}");
                Console.WriteLine($"Poin : {poin1}");
            }
            else if (pulsa >= 25000 && pulsa < 50000)
            {
                Console.WriteLine($"Pulsa : {pulsa}");
                Console.WriteLine($"Poin : {poin2}");
            }
            else if (pulsa >= 50000 && pulsa < 100000)
            {
                Console.WriteLine($"Pulsa : {pulsa}");
                Console.WriteLine($"Poin : {poin3}");
            }
            else if (pulsa == 100000)
            {
                Console.WriteLine($"Pulsa : {pulsa}");
                Console.WriteLine($"Poin : {poin4}");
            }
            else
            {
                Console.WriteLine("Tidak mendapatkan poin");
            }
        }

        static void soalSatu()
        {
            Console.WriteLine("---Grade Mahasiswa---");
            Console.Write("Masukkan grade mahasiswa : ");
            int nilai = int.Parse(Console.ReadLine());
            int nilaiMax = 100;

            if (nilai >= 90 && nilai <= nilaiMax)
            {
                Console.WriteLine("Grade A");
            }
            else if (nilai >= 70 && nilai < 90)
            {
                Console.WriteLine("Grade B");
            }
            else if (nilai >= 50 && nilai < 70)
            {
                Console.WriteLine("Grade C");
            }
            else if (nilai < 50 && nilai >= 0)
            {
                Console.WriteLine("Nilai E");
            }
            else
            {
                Console.WriteLine("Masukkan nilai yang sesuai");
            }
        }

           //}

        //}
    }
}


