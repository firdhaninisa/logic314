﻿

using System;

namespace Logic07_pr2
{
    class Program
    {
        static void Main(string[] args)
        {
            //duabelas_Logic07();
            //sepuluh_Logic07();
            //sembilan_Logic07();
            //delapan_Logic07_2();
            //delapan_Logic07();            
            //tujuh_Logic07();
            //enam_Logic07();
            //lima_Logic07();
            //empat.Logic07();
            //tiga_Logic07();
            //dua_Logic07();
            satu_Logic07();

            Console.ReadKey();
        }

        static void duabelas_Logic07()
        {
            Console.WriteLine("----------Sorting Number----------");
            //int angka;
            //int[] arrayAngka = new int[100];
            //Console.Write("Masukkan angka : ");
            //angka = int.Parse(Console.ReadLine().Split(","), int.Parse);
            Console.WriteLine("Masukkan Angka : ");
            int[] arrayAngka = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            for (int i = 0; i < arrayAngka.Length - 1; i++)
            {
                for (int j = 0; j < arrayAngka.Length - 1; j++)
                {
                    if (arrayAngka[j] > arrayAngka[j+1])
                    {
                        int temp = arrayAngka[j];
                        arrayAngka[j] = arrayAngka[j + 1];
                        arrayAngka[j + 1] = temp;
                    }
                }
            }
            Console.WriteLine("Output : ");
            Console.WriteLine(String.Join(",", arrayAngka));
        }

        static void sebelas_Logic07()
        {
            Console.WriteLine("----------Memindahkan angka pertama ke akhir----------");
            Console.Write("Input angka : ");
            int[] angka = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Console.Write("Jumlah rotasi : ");
            int rotasi = int.Parse(Console.ReadLine());
            int rot = 0;

            for(int i = 0; i <= rotasi; i++)
            {
                for(int j = 0; j < rotasi; j++)
                {
                    if (j < 1)
                    {
                        rot = angka[j];
                        angka[j] = angka[j + 1];
                    }
                    else if(j >= 1 && j < angka.Length - 1)
                    {
                        angka[j] = angka[j + 1];
                    }
                    else
                    {
                        angka[angka.Length - 1] = rot;
                    }
                }
                Console.Write(String.Join(" ", angka));
            }
        }

        static void sepuluh_Logic07()
        {
            Console.WriteLine("----------Menghitung lilin yang tertiup----------");
            Console.Write("Masukkan panjang lilin\t: ");
            int[] lilin = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int maks = 0, banyakmaks = 0;

            for (int i = 0; i < lilin.Length; i++)
            {
                if (lilin[i] > maks)
                {
                    maks = lilin[i];
                }
            }

            for (int j = 0; j < lilin.Length; j++)
            {
                if (lilin[j] == maks)
                    banyakmaks++;
            }
            Console.WriteLine($"Nilai lilin tertinggi adalah {maks}");
            Console.WriteLine($"Jumlah lilin tertinggi sebanyak {banyakmaks}");
        }

        static void sembilan_Logic07()
        {
            //int[,] angka = new int[3, 3];
            int[,] angka =
            {
                {11, 2, 4},
                {4, 5, 6},
                {10, 8, -12}
            };
            int diagonal1 = angka[0, 0] + angka[1, 1] + angka[2, 2];
            Console.WriteLine($"Jumlah angka di diagonal pertama\t: {angka[0, 0]} + {angka[1, 1]} + {angka[2, 2]}\t= {diagonal1}");

            int diagonal2 = angka[0, 2] + angka[1, 1] + angka[2, 0];
            Console.WriteLine($"Jumlah angka di diagonal kedua\t\t: {angka[0, 2]} + {angka[1, 1]} + {angka[2, 0]}\t= {diagonal2}");

            int perbedaan = (diagonal1 - diagonal2);
            Console.WriteLine($"Perbedaan\t\t\t\t: {diagonal1} - {diagonal2}\t= {perbedaan}");
        }

        static void delapan_Logic07_2()
        {

            Console.WriteLine("----------Star Case----------");
            Console.Write("Masukkan sampel input: ");
            int sampel = int.Parse(Console.ReadLine());

            for (int i = 0; i < sampel; i++)
            {
                for (int j = 0; j < sampel; j++)
                {
                    if (j < sampel - 1 - i)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
                Console.WriteLine("");
            }
        }

        static void delapan_Logic07()
        {
            Console.WriteLine("-----Star Case-----");
            Console.Write("Masukkan sampel input\t: ");
            int sampel = int.Parse(Console.ReadLine());

            int i, j, k;
            for (i = 1; i <= sampel; i++)
            {
                for (j = 1; j <= sampel - i; j++)
                {
                    Console.Write(" ");
                }
                for (k = 1; k <= i; k++)
                {
                    Console.Write("*");
                }
                Console.Write("");
            }
            Console.ReadLine();
        }

        static void tujuh_Logic07()
        {
            Console.WriteLine("----------Makanan----------");
            int uang = 0, totalHargaMenu = 0, bisaDimakan = 0, totalBayar = 0, sisa = 0;

            Console.Write("Masukkan total menu\t: ");
            int totalMenu = int.Parse(Console.ReadLine());
            Console.Write("Masukkan index makanan alergi\t: ");
            int indexMakananAlergi = int.Parse(Console.ReadLine());
        ulang:
            Console.Write("Masukkan harga menu\t: ");
            int[] hargaMenu = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            if (hargaMenu.Length > totalMenu)
            {
                Console.WriteLine("Index inputan berlebih, ulangi lagi");
                goto ulang;
            }
            else
            {
                Console.Write("Masukkan uang Anda\t: ");
                uang = int.Parse(Console.ReadLine());

                for (int i = 0; i < hargaMenu.Length; i++)
                {
                    totalHargaMenu += hargaMenu[i];
                }

                bisaDimakan = totalHargaMenu - hargaMenu[indexMakananAlergi];
                totalBayar = bisaDimakan / 2;
                sisa = uang - totalBayar;

                if (sisa > 0)
                {
                    Console.WriteLine($"Anda harus membayar = {totalBayar.ToString("Rp #,##")}");
                    Console.WriteLine($"Sisa uang Anda = {sisa.ToString("Rp #,##")}");
                }
                else if (sisa == 0)
                {
                    Console.WriteLine("Uang Anda pas tidak ada kembalian");
                }
                else
                {
                    Console.WriteLine($"Uang Anda kurang {sisa.ToString("Rp #,##")}");
                }
            }
        }

        static void enam_Logic07()
        {
            int i;
            Console.WriteLine("----------Split Kata----------");
            Console.Write("Masukkan kata: ");
            //string kata = Console.ReadLine();
            //char[] huruf = kata.Split(' ');
            char[] kalimat = Console.ReadLine().ToLower().Replace(" ", "").ToCharArray();

            for (i = 0; i < kalimat.Length; i++)
            {
                Console.WriteLine($"***{kalimat[i]}***");
            }
        }

        static void lima_Logic07()
        {
            string kalimat;
            Console.WriteLine("----------Menghitung jumah huruf vokal dan konsonan----------");
            Console.Write("Masukkan kalimat: ");
            kalimat = Console.ReadLine().ToLower();
            int vokal = 0, konsonan = 0;

            foreach (char huruf in kalimat)
            {
                if (huruf >= 'a' && huruf <= 'z')
                {
                    if (huruf == 'a' || huruf == 'i' || huruf == 'u' || huruf == 'e' || huruf == 'o')
                    {
                        vokal++;
                    }
                    else
                    {
                        konsonan++;
                    }
                }
            }
            Console.WriteLine($"Jumlah huruf vokal\t: {vokal}");
            Console.WriteLine($"Jumlah huruf konsonan\t: {konsonan}");
        }

        //static void empat_Logic07()
        //{
        //    /*Console.WriteLine("Tanggal Mulai (dd/mm/yyyy)\t= ");
        //    Console.WriteLine("Hari Libur\t= ");
        //    string strTanggal = Console.ReadLine();

        //    DateTime tanggal = DateTime.Parse(strTanggal);*/
        //    Console.WriteLine("----------Cek Tanggal Ujian----------");
        //    Console.Write("Masukkan Tanggal mulai (dd/mm/yy) : ");
        //    DateTime dateStart = DateTime.Parse(Console.ReadLine());

        //    Console.Write("Masukkan berapa hari ujian : ");
        //    int exam = int.Parse(Console.ReadLine());

        //    Console.Write("Masukkan hari libur : ");
        //    int[] holiday = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

        //    DateTime tanggalSelesai = dateStart;

        //    for(int i=0; i < exam; i++)
        //    {
        //        if (i > 0)
        //        {
        //            tanggalSelesai = tanggalSelesai.AddDays(1);
        //        }
        //        if (tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
        //        {
        //            tanggalSelesai = tanggalSelesai.AddDays(2);
        //        }
        //    }

        //    for (int j = 0; j < holiday.Length; j++)
        //    {
        //        if (tanggalSelesai.Day == holiday[j])
        //        {
        //            tanggalSelesai = tanggalSelesai.AddDays(1);
        //            if (tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
        //            {
        //                tanggalSelesai = tanggalSelesai.AddDays(2);
        //            }
        //        }
        //    }

        //    DateTime tanggalUjian = tanggalSelesai.AddDays(1);
        //    if (tanggalUjian.DayOfWeek == DayOfWeek.Saturday)
        //    {
        //            tanggalUjian = tanggalUjian.AddDays(2);
        //        }
        //        Console.WriteLine();
        //        Console.WriteLine("Kelas akan ujian pada Tanggal : " + tanggalUjian.ToString("dddd, dd/MM/yyyy"));
        //    }
        //}

        static void tiga_Logic07()
        {
            Console.WriteLine("----------Peminjaman Buku----------");
            Console.Write("Masukkan tanggal peminjaman (dd/mm/yy)\t\t: ");
            string strtanggal1 = Console.ReadLine();
            DateTime tglPinjam = DateTime.Parse(strtanggal1);

            Console.Write("Masukkan tanggal pengembalian (dd/mm/yy)\t: ");
            string strtanggal2 = Console.ReadLine();
            DateTime tglKembali = DateTime.Parse(strtanggal2);

            TimeSpan jmlHari = tglKembali - tglPinjam;
            int jumlahHari = jmlHari.Days;

            if (jumlahHari > 3)
            {
                Console.WriteLine($"Lama peminjaman buku\t= {jmlHari.Days} hari");
                Console.WriteLine($"Denda peminjaman buku\t= Rp.{(jmlHari.Days - 3) * 500}");
            }
            else if (jumlahHari <= 3 && jumlahHari > 0)
            {
                Console.WriteLine($"Lama peminjaman buku\t= {jmlHari.Days} hari");
                //Console.WriteLine($"Denda peminjaman buku\t= Rp.{(jmlHari.Days - 3) * 500}");
                Console.WriteLine("Tidak ada denda");
            }
            else
            {
                Console.WriteLine("Masukkan data yang sesuai");
            }

        }
        static void dua_Logic07()
        {
            Console.WriteLine("----------Sinyal SOS----------");
            Console.Write("Masukkan kode sinyal : ");
            char[] sinyal = Console.ReadLine().ToUpper().ToCharArray();

            int count = 0;
            int countB = 0;

            string tmp = "";

            if (sinyal.Length % 3 != 0)
            {
                Console.WriteLine("Invalid! Masukkan kode yang benar...");
            }

            for (int i = 0; i < sinyal.Length; i += 3)
            {
                if (sinyal[i] != 'S' || sinyal[i + 1] != 'O' || sinyal[i + 2] != 'S')
                {
                    count++;
                    tmp += "SOS";
                }
                else
                {
                    tmp += sinyal[i].ToString() + sinyal[i + 1].ToString() + sinyal[i + 2].ToString();
                    countB++;
                }
            }
            Console.WriteLine();
            Console.WriteLine($"Total sinyal salah adalah\t: {count}");
            Console.WriteLine($"Total sintal benar adalah\t: {countB}");
            Console.WriteLine($"Sinyal yang diterima\t\t: {String.Join("", sinyal)}");
            Console.WriteLine($"Sinyal yang benar\t\t: {tmp}");

        }

        /*static void satu_Logic07_2()
        {
            Console.WriteLine("----------Faktorial----------");
            Console.Write("Masukkan jumlah anak : ");
            int anak = int.Parse(Console.ReadLine());

            Console.Write($"{anak}! = ");
            int hasil = 1;

        }*/

        static void satu_Logic07()
        {
            int angka, faktorial = 1;
            Console.WriteLine("----------Faktorial----------");
            Console.Write("Masukkan angka : ");
            angka = int.Parse(Console.ReadLine());

            Console.Write($"{angka}! = ");
            int hasil = 1;

            for (int i = 1; i <= angka; i++)
            {
                faktorial = faktorial * i;

                Console.Write(i);
                if (i > 1)
                {
                    Console.Write(" * ");
                }
                else
                {

                    Console.WriteLine($"x = {angka}");
                    //Console.WriteLine($"{angka}! = )
                    Console.WriteLine($"Ada {faktorial} cara");
                }
            }
        }
    }
}