﻿using System;

namespace Logic09
{
    class Program
    {
        static void Main(string[] args)
        {
            //satu_Logic09();
            //dua_Logic09();
            //tiga_Logic09();
            tiga_Logic09_2();
            Console.ReadKey();
        }

        static void satu_Logic09()
        {
            double tokoToCust = 2;
            double cust1ToCust2 = 0.5;
            double cust2ToCust3 = 1.5;
            double cust3ToCust4 = 0.3;

            Console.WriteLine("----------Jumlah Bensin Ojol yang Dikeluarkan----------");
            Console.Write("Input : ");
            double input = double.Parse(Console.ReadLine());

            if(input == 1)
            {
                Console.WriteLine($"Jarak Tempuh = {tokoToCust} km");
                Console.WriteLine($"Bensin = {Math.Round(((tokoToCust / 2.5) * 1), MidpointRounding.ToPositiveInfinity)} liter");
            }
            else if(input == 2)
            {
                double duaCust = tokoToCust + cust1ToCust2;
                Console.WriteLine($"Jarak Tempuh = {tokoToCust} km + {cust1ToCust2} m = {duaCust} km");
                Console.WriteLine($"Bensin = {Math.Round(((duaCust / 2.5) * 1), MidpointRounding.ToPositiveInfinity)} liter");
            }
            else if(input == 3)
            {
                double tigaCust = tokoToCust + cust1ToCust2 + cust2ToCust3;
                Console.WriteLine($"Jarak Tempuh = {tokoToCust} km + {cust1ToCust2} m + {cust2ToCust3} km = {tigaCust} km");
                Console.WriteLine($"Bensin = {Math.Round(((tigaCust / 2.5) * 1), MidpointRounding.ToPositiveInfinity)} liter");
            }
            else if(input == 4)
            {
                double empatCust = tokoToCust + cust1ToCust2 + cust2ToCust3 + cust3ToCust4;
                Console.WriteLine($"Jarak Tempuh = {tokoToCust} km + {cust1ToCust2} m + {cust2ToCust3} km + {cust3ToCust4} m = {empatCust} km");
                Console.WriteLine($"Bensin = {Math.Round(((empatCust / 2.5) * 1), MidpointRounding.ToPositiveInfinity)} liter");
            }
            else
            {
                Console.WriteLine("Input dengan nilai sesuai");
            }
        }

        static void dua_Logic09()
        {
            Console.Write("----------Bahan yang dibutuhkan untuk membuat n-pukis----------");
            double pukis = double.Parse(Console.ReadLine());

            double terigu = 115;
            double gulaPasir = 190;
            double susu = 100;

            double jmlTerigu = (pukis * terigu) / 15;
            double jmlGulaPasir = (pukis * gulaPasir) / 15;
            double jmlSusu = (pukis * susu) / 15;

            Console.WriteLine($"Bahan yang dibutuhkan untuk membuat {pukis} kue pukis :");
            Console.WriteLine($"Terigu\t\t: {jmlTerigu} g");
            Console.WriteLine($"Gula Pasir\t: {jmlGulaPasir} kg");
            Console.WriteLine($"Susu\t\t: {jmlSusu} liter");
        }

        static void tiga_Logic09()
        {
            
            Console.Write("Masukkan nilai N\t: ");
            //int[] n = Array.ConvertAll(Console.ReadLine().Split("\t"), int.Parse);
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukkan kelipatan\t: ");
            int kelipatan = int.Parse(Console.ReadLine());

            int tampung = 0;

            /*for (int i = 0 ; i < n ; i++)
            {
                for(int j = 0 ; j < n ; j++)
                {
                    if (j == 0 && i >= 0 && i < n)
                    {
                        tampung = kelipatan;
                        Console.Write(tampung);
                    }
                    else if (j == i && j < n)
                    {
                        tampung += kelipatan * j;
                        Console.Write(tampung);
                    }
                    else if (i == n-1 && j >= 0 && j <= n-1)
                    {
                        tampung += kelipatan;
                        Console.Write(tampung);
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }*/

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (j == 0 && i >= 0 && i < n)
                    {
                        tampung = kelipatan;
                        Console.Write(tampung + "\t");
                    }
                    else if (i == n-1)
                    {
                        tampung += kelipatan;
                        Console.Write(tampung + "\t");
                    }
                    else if (j == i)
                    {
                        tampung += kelipatan * j;
                        Console.Write(tampung + "\t");
                    }
                  
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine();
            }
        }
        static void tiga_Logic09_2()
        {
            Console.Write("Masukkan banyak suku\t: ");
            int suku = int.Parse(Console.ReadLine());
            Console.Write("Masukkan angka awal\t: ");
            int angka = int.Parse(Console.ReadLine());

            for(int i = 0; i < suku; i++)
            {
                int x = 0;
                for(int j = 0; j < suku; j++)
                {
                    x += angka;
                    if (j == 0 || i == suku - 1 || i == j)
                        Console.Write(x.ToString() + "\t");
                    else
                        Console.Write("\t");
                }
                Console.WriteLine();
            }

        }

    }
}
