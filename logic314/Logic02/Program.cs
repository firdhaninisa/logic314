﻿using System;

namespace Logic02
{
    class Program
    {
        static void Main(string[] args)
        {
            //ifStatement();
            //elseStatement();
            //ifNested();
            //ternary();
            //switchCase();
            //soalSatu();
            //soalDua();
            lingkaran();

            Console.ReadKey();
        }
        //static double hitungKelilingPersegi (int sisi)
        //{
            //int kelilingPersegi = 0;
            //int kelilingPersegi = 4 * sisi;
            //return kelilingPersegi;

        //}

        static void persegi()
        {

            

        }
        static double hitungKeliling(int jari, double phi)
        {
            double keliling = 0;
            keliling = phi * 2 * jari;
            return keliling;
           
        }
        static double hitungLuas(int jari, double phi)
        {
            double luas = 0;
            luas = phi * jari * jari;
            return luas;

        }


        static void lingkaran()
        {
            int jari;
            double phi = 3.14;

            Console.WriteLine("--Method ReturnType Lingkaran--");
            Console.Write("Masukkan jari-jari = ");
            jari = Convert.ToInt32(Console.ReadLine());
        
            
            double keliling = hitungKeliling(jari, phi);
            double luas = hitungLuas(jari, phi);

            Console.WriteLine($"Hasil keliling = {keliling}");
            Console.WriteLine($"Hasil luas = {luas}");
        }

        

        static void soalDua()
        {
            int sisi;
            int luas;
            int keliling;

            Console.WriteLine("Masukkan panjang sisi : ");
            sisi = int.Parse(Console.ReadLine());

            luas = sisi * sisi;
            keliling = 4 * sisi;

            Console.WriteLine("Luas persegi adalah " + luas);
            Console.WriteLine("Keliling persegi adalah " + keliling);

        }

        static void soalSatu()
        {

            const double phi = 3.14;
            double luas;
            double keliling;
            int jari;

            Console.Write("Masukkan jari-jari lingkaran : ");
            jari = int.Parse(Console.ReadLine());

            luas = phi * jari * jari;
            keliling = phi * 2 * jari;

            Console.WriteLine("Luas lingkaran adalah " + luas);
            Console.WriteLine("Keliling lingkaran adalah " + keliling);

        }

        static void switchCase()
        {
            Console.WriteLine("--Switch Case");
            Console.Write("Pilih buah kesukaan kalian (apel, mangga, pisang): ");
            string input = Console.ReadLine().ToLower();

            switch (input)
            {
                case "apel":
                    Console.WriteLine("Anda memilih buah apel");
                    break;
                case "mangga":
                    Console.WriteLine("Anda memilih buah mangga");
                    break;
                case "pisang":
                    Console.WriteLine("Anda memilih buah pisang");
                    break;
                default:
                    Console.WriteLine("Anda memilih buah yang lain");
                    break;
            }
        }

        static void ternary()
        {
            Console.WriteLine("--Ternary--");
            int x, y;
            Console.Write("Masukkan nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y : ");
            y = int.Parse(Console.ReadLine());

            string hasilTernary = x > y ? "Nilai x lebih dari nilai y" :
                x < y ? "Nilai x lebih kecil dari nilai y" : "Nilai x sama dengan nilai y";

            Console.WriteLine(hasilTernary);
        }

        static void ifNested()
        {
            Console.WriteLine("--If Nested--");
            Console.Write("Masukkan nilai : ");
            int nilai = int.Parse(Console.ReadLine());
            int maxNilai = 100;

            if(nilai >= 70 && nilai <= maxNilai )
            {
                Console.WriteLine("Kamu berhasil");
                if(nilai == 100)
                {
                    Console.WriteLine("Kamu keren");
                }
            else if (nilai >= 0 && nilai < 70)
                {
                    Console.WriteLine("Kamu tidak berhasil");
                }
            }
            else
            {
                Console.WriteLine("Masukkan nilai yang sesuai");
            }

        }

        static void elseStatement()
        {
            Console.WriteLine("--Else Statement--");
            int x, y;
            Console.Write("Masukkan nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y : ");
            y = int.Parse(Console.ReadLine());

            if (x > y)
            {
                Console.WriteLine("Nilai x lebih besar dari nilai y");
            }
            else if (y > x)
            {
                Console.WriteLine("Nilai y lebih besar dari nilai x");
            }
            else
            {
                Console.WriteLine("Nilai x sama dengan nilai y");
            }
        }
  

        static void ifStatement()
        {
            Console.WriteLine("--If Statement--");
            int x, y;
            Console.Write("Masukkan nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y : ");
            y = int.Parse(Console.ReadLine());

            if (x > y)
            {
                Console.WriteLine("Nilai x lebih besar dari nilai y");
            }
            if (y > x)
            {
                Console.WriteLine("Nilai y lebih besar dari nilai x");
            }
            if (x == y)
            {
                Console.WriteLine("Nilai x sama dengan nilai y");
            }
        }
    }
}
