﻿using System;
using System.Collections.Generic;

namespace Logic07
{
    class Mamalia
    {
        public virtual void pindah()
        {
            Console.WriteLine("Kucing Lari......");
        }
    }
    class Kucing : Mamalia
    {

    }
    class Paus : Mamalia
    {
        public override void pindah()
        {
            Console.WriteLine("Paus Berenang...");
        }
    }
    public class Program
    {
        static readonly List<User> listUsers = new List<User>();
        static void Main(string[] args)
        {
            //Combination2();
            //overriding();
            //classInheritence();
            //timeSpan();
            //stringDateTime();
            //dateTime();
            
            //bool ulangi = true;
            //while (ulangi)
            //{
            //    insertUser();
            //}
            
            ////listUser();
            
            Console.ReadKey();
        }
        
        static void Combination2()
        {
            Logic07_pr combination = new Logic07_pr();
            
        }
        
        static void overriding()
        {
            Paus paus = new Paus();
            paus.pindah();

            Kucing kucing = new Kucing();
            kucing.pindah();
        }       
        static void classInheritence()
        {
            TypeMobil typeMobil = new TypeMobil();
            typeMobil.kecepatan = 10;
            typeMobil.posisi = 50;
            typeMobil.Civic();
        }
        
        static void timeSpan() //interval waktu (ada 'start' dan 'and'
        {
            DateTime date1 = new DateTime(2023, 1, 1, 0, 0, 0);
            DateTime date2 = DateTime.Now;

            //TimeSpan span = date1.Subtract(date2);
            TimeSpan span = date2 - date1;

            Console.WriteLine($"Total Hari\t: {span.Days}");
            Console.WriteLine($"Total Hari\t: {span.TotalDays}");
            Console.WriteLine($"Interval Jam\t: {span.Hours}");
            Console.WriteLine($"Total Jam\t: {span.TotalHours}");
            //Console.WriteLine($"Total Menit\t: {span.Minutes}");
            Console.WriteLine($"Total Menit\t: {span.TotalMinutes}");
            //Console.WriteLine($"Total Detik\t: {span.Seconds}");
            Console.WriteLine($"Total Detik\t: {span.TotalSeconds}");
        }
        static void stringDateTime()
        {
            Console.WriteLine("--String DateTime--");
            Console.Write("Masukkan tanggal (dd/mm/yy) : ");
            string strtanggal = Console.ReadLine();

            DateTime tanggal = DateTime.Parse(strtanggal);

            Console.WriteLine($"Tanggal\t\t: {tanggal.Day}");
            Console.WriteLine($"Bulan\t\t: {tanggal.Month}");
            Console.WriteLine($"Tahun\t\t: {tanggal.Year}");
            Console.WriteLine($"DayofWeek ke\t: {(int)tanggal.DayOfWeek}"); //hari ke-
            Console.WriteLine($"DayofWeek ke\t: {tanggal.DayOfWeek}"); // hari
        }
        static void dateTime()
        {
            Console.WriteLine("--DateTime--");
            DateTime dt1 = new DateTime(); // 01/01/0001 00:00:00
            Console.WriteLine(dt1);

            DateTime dt2 = DateTime.Now; //waktu sekarang
            Console.WriteLine(dt2);

            DateTime d3 = DateTime.Now.Date; //tanggal sekarang tanpa time
            Console.WriteLine(d3);

            DateTime dt4 = DateTime.UtcNow;
            Console.WriteLine(dt4);

            DateTime dt5 = new DateTime(2023, 03, 09);
            Console.WriteLine(dt5);

            DateTime dt6 = new DateTime(2023, 3, 9, 11, 45, 00);
            Console.WriteLine(dt6);
        }
        static void insertUser()
        {
            Console.WriteLine("---Insert User---");
            Console.Write("Masukkan Nama\t: ");
            string nama = Console.ReadLine();
            Console.Write("Masukkan Umur\t: ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Alamat\t: ");
            string alamat = Console.ReadLine();

            List<User> listUser = new List<User>();

            User user = new User();
            user.Nama = nama;
            user.Umur = umur;
            user.Alamat = alamat;

            listUsers.Add(user);

            for (int i = 0; i < listUsers.Count; i++)
                Console.WriteLine($"Nama : {listUsers[i].Nama}\tUmur :{listUsers[i].Umur}\tAlamat :{listUsers[i].Alamat}");
        }
        static void listUser()
        {
            List<User> listUser = new List<User>()
            {
                new User(){Nama = "Firdha", Umur = 24, Alamat = "Tangsel"},
                new User(){Nama = "Isni", Umur = 22, Alamat = "Cimahi"},
                new User(){Nama = "Asti", Umur = 23, Alamat = "Garut"},
                new User(){Nama = "Muafa", Umur = 22, Alamat = "Bogor"},
                new User(){Nama = "Toni", Umur = 24, Alamat = "Garut"}
            };
            for (int i = 0; i < listUser.Count; i++)
                Console.WriteLine($"Nama : {listUser[i].Nama}\tUmur :{listUser[i].Umur}\tAlamat :{listUser[i].Alamat}");               
        }
    }
}
