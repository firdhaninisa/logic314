﻿using System;

namespace logic04
{
    class Program
    {
        static void Main(string[] args)
        {
            //removeString();
            //insertString();
            Console.ReadKey();
        }
        
        static void replaceString()
        {
            Console.WriteLine("--Replace String--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Dari kata : ");
            string kataLama = Console.ReadLine();
            Console.Write("Replace menjadi : ");
            string kataBaru = Console.ReadLine();

            Console.Write($"Hasil Insert String : {kalimat.Replace(kataLama, kataBaru)}");
        }
        
        static void insertString()
        {
            Console.WriteLine("--Insert String--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Isi parameter Insert : ");
            int param = int.Parse(Console.ReadLine());
            Console.Write("Masukkan input string: ");
            string input = Console.ReadLine();

            Console.Write($"Hasil Insert String : {kalimat.Insert(param, input)}");
        }

        static void removeString()
        {
            Console.WriteLine("--Remove String--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Isi parameter Remove : ");
            int param = int.Parse(Console.ReadLine());

            Console.Write($"Hasil Remove String : {kalimat.Remove(param)}");
        }
    }
}
