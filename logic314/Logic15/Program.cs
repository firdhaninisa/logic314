﻿using System;
using System.Collections.Generic;

namespace Logic15
{
    class Program
    {
        static void Main(string[] args)
        {
            //Satu();
            //Dua();
            //Dua_2();
            //Tiga();
            //Empat();
            //Lima();
            //Enam();
            //Enam_2();
            //Tujuh();
            //Delapan();
            //Sembilan();
            //Sepuluh();
            //SoalKisiKisi_1();
            //SoalKisiKisi_2();
            
            Console.ReadKey();
        }

        static void SoalKisiKisi_1()
        {
            //List<int> list = new List<int>() 
            //{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);

            for(int i = 0; i < list.Count; i++)
            {
                if (list[i] % 2 == 0)
                    list.RemoveAt(i);               
            }
            Console.WriteLine(String.Join(",", list));
        }

        static void SoalKisiKisi_2()
        {
            Console.WriteLine("----------Bilangan Prima----------");
            Console.Write("Masukkan angka : ");
            int bilangan = int.Parse(Console.ReadLine());


            bool prima = true;
            int tampung = 0;
            if (bilangan >= 2)
            {
                
                //for(int i = 2; i <= bilangan; i++)
                //{
                for(int j = 2; j < bilangan; j++)
                {
                    if(bilangan % j == 0)
                    {
                        tampung = j;
                        prima = false;
                        break; //apabila ada break, untuk menunjukkan pembagi terkecil// apabila tidak ada, menampilkan pembagi terbesar
                    }
                }
                if (prima)
                {
                    Console.WriteLine($"1");
                }
                else
                {
                    Console.WriteLine(tampung);
                }
                prima = true;
                //}
            }
            else
            {
                Console.WriteLine("Inputan bukan bilangan prima");
            }
        }

        static void Satu()
        {
            Console.Write("Input kalimat\t: ");
            string kalimat = Console.ReadLine();

            int hasil = 0;

            for (int i = 0; i < kalimat.Length; i++)
            {
                if (char.IsUpper(kalimat[i]))
                {
                    hasil++;
                }
            }
            Console.WriteLine($"Output : {hasil}");
        }

        static void Dua()
        {

            Console.WriteLine("----------Nomor Invoice Penjualan----------");

            Console.Write("Start\t: ");
            int start = int.Parse(Console.ReadLine());
            Console.Write("End\t: ");
            int end = int.Parse(Console.ReadLine());

            /*DateTime dt = new DateTime(2022, 08, 07);
            int tanggal = dt.Day;
            int bulan = dt.Month;
            int tahun = dt.Year;*/

            /*string dateString = "07/08/2022";
            DateTime dt1 = DateTime.Parse(dateString);
            int tanggal = dt1.Day;
            int bulan = dt1.Month;
            int tahun = dt1.Year;*/


            DateTime dt2 = DateTime.Now;

            int tanggal = dt2.Day;
            int bulan = dt2.Month;
            int tahun = dt2.Year;

            for (int i = start; i <= end; i++)
            {
                Console.WriteLine($"XA-{tanggal}{bulan}{tahun}-{i.ToString().PadLeft(5, '0')}");
            }
        }

        static void Dua_2()
        {
            Console.WriteLine("----------Nomor Inovoice Penjualan---------");
            Console.Write("Masukkan start : ");
            int start = int.Parse(Console.ReadLine());
            Console.Write("Masukkan end : ");
            int end = int.Parse(Console.ReadLine());

            string initial = "XA-";

            DateTime date = DateTime.Now;

            for(int i =  start; i <= end; i++)
            {
                Console.WriteLine($"{initial}{date.ToString("ddMMyyy")}-{i.ToString().PadLeft(5, '0')}");
            }
        }

        static void Tiga()
        {
            Console.WriteLine("----------Jumlah Buah yang Ada di Dapur----------");
            Console.Write("Keranjang 1\t: ");
            int ker_1 = int.Parse(Console.ReadLine());
            Console.Write("Keranjang 2\t: ");
            int ker_2 = int.Parse(Console.ReadLine());
            Console.Write("Keranjang 3\t: ");
            int ker_3 = int.Parse(Console.ReadLine());
            Console.Write("Keranjang yang dibawa ke pasar : ");
            int ker_psr = int.Parse(Console.ReadLine());

            int tampung = 0;

            switch (ker_psr)
            {
                case 1:
                    tampung = ker_2 + ker_3;
                    break;
                case 2:
                    tampung = ker_1 + ker_3;
                    break;
                case 3:
                    tampung = ker_1 + ker_2;
                    break;
                default:
                    Console.Write("Pilih antara 1 sampai 3: ");
                    break;
            }

            Console.WriteLine($"Sisa Buah : {tampung}");

        }

        static void Tiga_2()
        {
            Console.WriteLine("----------Jumlah Buah yang Ada di Dapur----------");


        ulang1:
            Console.Write("Keranjang 1\t: ");
            string ker_1 = Console.ReadLine();
            Console.Write("Keranjang 2\t: ");
            string ker_2 = Console.ReadLine();
            Console.Write("Keranjang 3\t: ");
            string ker_3 = Console.ReadLine();
        ulang:
            Console.Write("Keranjang yang dibawa ke pasar (1-3): ");
            string ker_psr = Console.ReadLine().ToLower();

            int tampung = 0;

            //int tampung_ker = 0;

            if (ker_1 == "kosong")
            {
                ker_1 = "0";
            }
            else if (ker_2 == "kosong")
            {
                ker_2 = "0";
            }
            else if (ker_3 == "kosong")
            {
                ker_3 = "0";
            }
            else
            {
                Console.WriteLine("Masukkan input yang sesuai (kosong/ angka)");
                goto ulang1;
            }

            switch (ker_psr)
            {
                case "1":
                    tampung = int.Parse(ker_2) + int.Parse(ker_3);
                    break;
                case "2":
                    tampung = int.Parse(ker_1) + int.Parse(ker_3);
                    break;
                case "3":
                    tampung = int.Parse(ker_1) + int.Parse(ker_2);
                    break;
                default:
                    Console.WriteLine("1 sampai 3 saja");
                    goto ulang;
            }

            Console.WriteLine($"Sisa Buah\t: {tampung}");

        }

        static void Empat()
        {
            Console.WriteLine("----------Donasi Baju----------");
            Console.Write("Input baju untuk: ");
            int input = int.Parse(Console.ReadLine());




        }

        static void Lima()
        {
            Console.WriteLine("----------Nilai Mahasiswa----------");
            Console.Write("Masukkan nilai mahasiswa : ");
            int[] nilai = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            for(int i = 0; i <= nilai.Length; i++)
            {
                //if()
                //{

                //}
            }
        }

        static void Enam()
        {
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            string huruf = "abcdefghijklmnopqrstuvwxyz";

            char[] huruf_array = huruf.ToCharArray();

            bool isPangram = true;
            for (int i = 0; i < huruf.Length; i++)
            {
                if (kalimat.Contains(huruf_array[i]) == false)
                {
                    isPangram = false;
                    break;
                }
            }
            if (isPangram)
            {
                Console.WriteLine("Pangram");
            }
            else
            {
                Console.WriteLine("not pangram");
            }
        }

        static void Enam_2()
        {
            Console.WriteLine("----------Pangram----------");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            string alfabet = "abcdefghijklmnopqrstuvwxyz";

            bool pangram = true;
            foreach(char item in alfabet)
            {
                if (!kalimat.Contains(item))
                {
                    pangram = false;
                    break;
                }
            }
            if (pangram)
            {
                Console.WriteLine("Kalimat ini adalah pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini bukan pangram");
            }
        }

        static void Tujuh()
        {
            Console.WriteLine("Masukkan himpunan angka : ");
            int angka = int.Parse(Console.ReadLine());

            int[] banyak_angka = new int[angka];

            for(int i = 0; i <= angka; i++)
            {
                /*if(i <= 1)
                {
                    banyak_angka[i] = 1;
                }*/
                //else
                //{
                    //banyak_angka[i] = banyak_angka[i - 2] + banyak_angka[i - 1];
                //}
            }
            Console.WriteLine(string.Join(",", banyak_angka));
        }

        static void Delapan()
        {

        }

        static void Sembilan()
        {
            Console.WriteLine("----------Transaksi Beli Pulsa----------");
            Console.Write("Masukkan jumlah pulsa\t: ");
            int pulsa = int.Parse(Console.ReadLine());

            int tampung = 0;
            if (pulsa >= 0 && pulsa <= 10000)
            {
                tampung = 0;
            }
            else if (pulsa > 10000 && pulsa <= 30000)
            {
                tampung = (pulsa - 10000) / 1000;
            }
            else if (pulsa > 30000)
            {
                tampung = 20 + (((pulsa - 30000) / 1000) * 2);
            }
            else
            {
                Console.WriteLine($"Nominal pulsa tidak terdaftar");
            }
            Console.WriteLine($"Poin Pulsa\t\t: {tampung}");

        }

        static void Sembilan_2()
        {
            Console.WriteLine("----------Poin Pulsa----------");
            Console.Write("Masukkan beli pulsa : ");
            int pulsa = int.Parse(Console.ReadLine());

            int point1 = 0, point2 = 0;

            if(pulsa <= 10000)
            {
                Console.WriteLine($"0 = 0 Point");
            }
            else if(pulsa <= 30000)
            {
                point1 = pulsa / 1000;
                Console.WriteLine($"0 + {point1} = {point1} point");
            }
            else
            {
                point1 = (30000 - 10000) / 1000;
                point2 = ((pulsa - 30000) / 1000) * 2;
                Console.WriteLine($"0 + {point1} + {point2} = {point1 + point2} point");
            }
        }

        static void Sepuluh()
        {
            Console.WriteLine("----------Nilai Target dengan Selisih Nilai Sama----------");
            Console.Write("Masukkan nilai target\t: ");
            int target = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai :");
            int[] nilai = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            int jumlah_angka = 0;
            for(int i = 0; i < nilai.Length; i++)
            {
                for(int j = 0; j < nilai.Length; j++)
                {
                    if(nilai[i] - nilai[j] == target)
                    {
                        jumlah_angka++;
                    }
                }
            }
            Console.WriteLine($"Output : {jumlah_angka}");
        }
    }
}
