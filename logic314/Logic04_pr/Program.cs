﻿using System;

namespace Logic04_pr
{
    class Program
    {
        static void Main(string[] args)
        {
            //soalEmpat();
            //Sepuluh();
            //Sembilan();
            //Delapan_3();
            //Delapan_2();
            //Delapan();
            //Tujuh();
            //Enam();
            //Lima();
            Empat_2();
            //Empat();
            //Tiga();
            //Dua();
            //Satu();
            Console.ReadKey();
        }

        static void Sepuluh()
        {
            Console.WriteLine("Harga Pakaian");
            Console.Write("Masukan kode baju: ");
            int kodeBaju = int.Parse((string)Console.ReadLine());
            Console.Write("Masukan ukuran baju: ");
            string ukBaju = Console.ReadLine();

            int harga = 0;
            string merkBaju;

            if (kodeBaju == 1)
            {
                merkBaju = "IMP";
                if (ukBaju == "S")
                {
                    harga = 200000;
                }
                else if (ukBaju == "M")
                {
                    harga = 220000;
                }
                else
                {
                    harga = 250000;
                }
            }
            else if (kodeBaju == 2)
            {
                merkBaju = "Prada";
                if (ukBaju == "S")
                {
                    harga = 150000;
                }
                else if (ukBaju == "M")
                {
                    harga = 160000;
                }
                else
                {
                    harga = 170000;
                }
            }
            else
            {
                merkBaju = "GUCCI";
                harga = 200000;
            }

            Console.WriteLine("Merk baju yang dipilih: " + merkBaju);
            Console.WriteLine("Harga baju yang dipilih: " + harga);
        }

        static void Sepuluh_2()
        {
            Console.Write("Masukkan angka : ");
            int n = int.Parse(Console.ReadLine());


        }
        
        static void Sembilan()
        {
            {
                Console.WriteLine("Konversi Waktu");
                Console.Write("Masukkan waktu sekarang: ");
                string waktu = Console.ReadLine();

                string PM = waktu.Substring(8, 2);
                int ubahWaktu = int.Parse(waktu.Substring(0, 2));

                if (PM == "PM" && ubahWaktu <= 12)
                {

                    ubahWaktu += 12;
                    Console.WriteLine("\t" + ubahWaktu.ToString() + waktu.Substring(2, 6));
                }
                else
                {
                    Console.WriteLine("\t" + waktu.Substring(0, 8));
                }

                Console.WriteLine();
            }

        }

        static void Sembilan_2()
        {
            Console.Write("Masukkan format waktu = ");
            string formatJam = Console.ReadLine().ToUpper();

            string pm = formatJam.Substring(8, 2);
            int jam = int.Parse(formatJam.Substring(0, 2));

            if (jam <= 12)
            {
                if (pm == "PM")
                {
                    jam += 12;
                    Console.WriteLine(jam.ToString() + formatJam.Substring(2, 6));
                }
                else
                {
                    Console.WriteLine(formatJam.Substring(0, 8));
                }
            }
            else
            {
                Console.WriteLine("Input yang bener");
            }
        }

        static void Delapan()
        {
            int n1 = 1, n2 = 1, n3, i, number;
            Console.Write("Masukkan banyak angka yang ingin dimunculkan : ");
            number = int.Parse(Console.ReadLine());
            Console.Write(n1 + " " + n2 + " ");

            for (i = 2; i < number; ++i)
            {
                n3 = n1 + n2;
                Console.Write(n3 + " ");
                n1 = n2;
                n2 = n3;
            }

        }

        static void Delapan_2()
        {
            int n1 = 1, n2 = 1, n3, i, number;
            Console.Write("Masukkan banyak angka yang ingin dimunculkan : ");
            number = int.Parse(Console.ReadLine());
            
            for (i = 0; i < number; ++i)
            {
                if (i <= 1)
                {
                    n3 = 1;
                }
                else
                {
                    n3 = n1 + n2;                    
                    n1 = n2;
                    n2 = n3;
                }
            Console.Write(n3 + " ");
            }
        }

        static void Delapan_3()
        {
            int number;
            Console.Write("Masukkan banyak angka yang ingin dimunculkan : ");
            number = int.Parse(Console.ReadLine());

            int[] numberArray = new int[number];

            for (int i = 0; i < number; ++i)
            {
                if (i <= 1)
                {
                    numberArray[i] = 1;                   
                }
                else
                {
                    numberArray[i] = numberArray[i-2]+ numberArray[i-1];                    
                }
                //Console.Write(numberArray[i] + " ");
            }
            Console.Write(string.Join(" ", numberArray));
        }

        static void Tujuh()
        {
            Console.WriteLine("Menambahkan simbol awal angka");
            Console.Write("Masukan jumlah bilangan angka: ");
            int range = int.Parse(Console.ReadLine());

            int angka = 0;
            for (int i = 1; i <= range; i++)
            {
                Console.Write("\t");
                angka += 5;
                string ubahAngka = angka.ToString();
                if (i % 2 == 1)
                {
                    Console.Write(ubahAngka.Insert(0, "-") + " ");
                }
                else
                {
                    Console.Write(" " + ubahAngka + " ");
                }

            }
            Console.WriteLine();
        }

        static void Enam()
        {
            Console.WriteLine("Replace ANgka");
            Console.Write("Masukan jumlah bilangan angka: ");
            int range = int.Parse(Console.ReadLine());

            int angka = 1;
            for (int i = 1; i <= range; i++)
            {
                angka *= 3;
                string ubahAngka = angka.ToString();

                if (i % 2 == 0)
                {

                    Console.Write(" " + ubahAngka.Replace(ubahAngka, "*") + " ");
                }
                else
                {
                    Console.Write(" " + ubahAngka + " ");
                }
            }
            Console.WriteLine();

            //int n1 = 3, n2 = 9, n3 = 27, i, number;
            //Console.Write("Masukkan banyak angka yang ingin dimunculkan : ");
            //number = int.Parse(Console.ReadLine());
            //Console.Write(n1 + " ");

            //for (i = 2; i < number; ++i)
            //{
            //    n2 = n1 * 3;
            //    n3 = n2 * 3;
            //    Console.Write(n3 + " ");
            //    n1 = n2;
            //    n2 = n3;
            //}
        }

        static void Lima()
        {
            Console.Write("Masukkan kalimat : ");
            //string[] kalimat = Console.ReadLine().Split(" ");
            string kalimat = Console.ReadLine();
            string[] kalimatArray = kalimat.Split(' ');


            for (int i = 0; i < kalimatArray.Length; i++)
            {
                Console.Write("\t");
                for (int j = 0; j < kalimatArray[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(kalimatArray[i].Remove(0));
                    }
                    else
                    {
                        Console.Write(kalimatArray[i][j]);
                    }

                }
                Console.Write(" ");
            }
        }

        static void Empat_2()
        {
            Console.WriteLine("-----Mengganti huruf di awal dan akhir");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            string[] kalimatArray = kalimat.Split(' ');

            string tampung = "";

            for(int i = 0; i < kalimatArray.Length; i++)
            {
                Console.Write("\t");
                for (int j = 0; j < kalimatArray.Length; j++)
                {
                    if (j == 0)
                    {
                        tampung += "*";
                    }
                    else if (j == kalimatArray[i].Length - 1)
                    {
                        tampung += "*";
                    }
                    else
                    {
                        tampung += kalimatArray[i][j];
                    }
                }
                tampung += " ";
            }
            Console.WriteLine(tampung);

            Console.WriteLine();
        }
        static void Empat()
        {
            Console.Write("Masukkan kalimat : ");
            //string[] kalimat = Console.ReadLine().Split(" ");
            string[] kalimat = Console.ReadLine().Split(" ");


            for (int i = 0; i < kalimat.Length; i++)
            {
                Console.Write("\t");
                for (int j = 0; j < kalimat[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(kalimat[i].Replace(kalimat[i], "*"));
                    }
                    else if (j == kalimat[i].Length - 1)
                    {
                        Console.Write(kalimat[i].Replace(kalimat[i], "*"));
                    }
                    else
                    {
                        Console.Write(kalimat[i][j]);
                    }

                }
                Console.Write(" ");

                //        char[] kataArray = kalimat.ToCharArray;
                //        Console.WriteLine(kataArray);
                //    }

            }
            //}
        }


        static void Tiga()
        {
            Console.Write("Masukkan kalimat : ");
            //string[] kalimat = Console.ReadLine().Split(" ");
            string[] kalimat = Console.ReadLine().Split(" ");


            for (int i = 0; i < kalimat.Length; i++)
            {
                Console.Write("\t");
                for (int j = 0; j < kalimat[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(kalimat[i][j]);
                    }
                    else if (j == kalimat[i].Length - 1)
                    {
                        Console.Write(kalimat[i][j]);
                    }
                    else
                    {
                        Console.Write(kalimat[i].Replace(kalimat[i], "*"));
                    }

                }
                Console.Write(" ");

                //        char[] kataArray = kalimat.ToCharArray;
                //        Console.WriteLine(kataArray);
                //    }

            }
            //}
        }

        static void Dua()
        {
            Console.WriteLine("---Menghitung Kalimat---");
            Console.Write("Masukkan kalimat : ");
            //string kalimat = Console.ReadLine();

            string[] kataArray = Console.ReadLine().Split();

            for (int i = 0; i < kataArray.Length; i++)
            {
                Console.WriteLine($"Kata ke-{i + 1} {kataArray[i]}");
            }
            Console.WriteLine($"Total kata adalah {kataArray.Length}");
        }


        static void Satu()
        {
            double upah = 0, lembur = 0, upahKaryawan = 0, upahLembur = 0, totalUpah = 0;
            Console.WriteLine("==========Gaji Karyawan Perusahaan Swasta==========");
            Console.Write("Golongan : ");
            int golongan = int.Parse(Console.ReadLine());
            Console.Write("Jam Kerja : ");
            int jamKerja = int.Parse(Console.ReadLine());

            if (golongan == 1 && jamKerja >= 0)
            {
                upah = 2000;
                if (jamKerja > 40)
                {

                    lembur = (jamKerja - 40);
                    upahKaryawan = upah * (jamKerja - lembur);
                    upahLembur = lembur * 1.5 * upah;
                }
                else
                {
                    upahKaryawan = upah * jamKerja;
                }
                totalUpah = upahKaryawan + upahLembur;
            }
            else if (golongan == 2 && jamKerja >= 0)
            {
                upah = 3000;
                if (jamKerja > 40)
                {

                    lembur = (jamKerja - 40);
                    upahKaryawan = upah * (jamKerja - lembur);
                    upahLembur = lembur * 1.5 * upah;
                }
                else
                {
                    upahKaryawan = upah * jamKerja;
                }
                totalUpah = upahKaryawan + upahLembur;
            }
            else if (golongan == 3 && jamKerja >= 0)
            {
                upah = 4000;
                if (jamKerja > 40)
                {

                    lembur = (jamKerja - 40);
                    upahKaryawan = upah * (jamKerja - lembur);
                    upahLembur = lembur * 1.5 * upah;
                }
                else
                {
                    upahKaryawan = upah * jamKerja;
                }
                totalUpah = upahKaryawan + upahLembur;
            }
            else if (golongan == 4 && jamKerja >= 0)
            {
                upah = 5000;
                if (jamKerja > 40)
                {

                    lembur = (jamKerja - 40);
                    upahKaryawan = upah * (jamKerja - lembur);
                    upahLembur = lembur * 1.5 * upah;
                }
                else
                {
                    upahKaryawan = upah * jamKerja;
                }
                totalUpah = upahKaryawan + upahLembur;
            }
            else
            {
                Console.WriteLine("Masukkan data dengan benar");
            }

            Console.WriteLine($"Upah : {upahKaryawan}");
            Console.WriteLine($"Lembur : {upahLembur}");
            Console.WriteLine($"Total : {totalUpah}");
        }


        static void soalEmpat()
        {
            //double diskonOngkir1, diskonOngkir2, diskonOngkir3, diskonOrder1, diskonOrder2, diskonOrder3;

            double order, voucherOrder, ongkirOrder, diskonOngkir = 0, diskonOrder = 0;



            Console.WriteLine("=====Promo Sopi=====");
            Console.Write("Belanja : ");
            order = double.Parse(Console.ReadLine());
            Console.Write("Ongkos Kirim = ");
            ongkirOrder = double.Parse(Console.ReadLine());
            Console.Write("Pilih voucher : ");
            voucherOrder = double.Parse(Console.ReadLine());




            //double diskonOngkir1 = 5000;
            //double diskonOngkir2 = 10000;
            //double diskonOngkir3 = 20000;

            //double diskonOrder1 = 5000;
            //double diskonOrder2 = 10000;
            //double diskonOrder3 = 10000;

            if (order >= 30000 && order < 50000 && voucherOrder == 1)
            {

                diskonOngkir = 5000;
                diskonOrder = 5000;
                //Console.WriteLine($"Belanja : {order}");
                //Console.WriteLine($"Ongkos Kirim : {ongkirOrder}");
                //Console.WriteLine($"Diskon Ongkir : {diskonOngkir1}");
                //Console.WriteLine($"Diskon Belanja : {diskonOrder1}");
                //Console.WriteLine($"Total Belanja : {order + ongkirOrder - diskonOrder1 - diskonOngkir1}");

            }

            else if (order >= 50000 && order < 100000)
            {
                if (voucherOrder == 1)
                {
                    diskonOngkir = 5000;
                    diskonOrder = 5000;

                }
                if (voucherOrder == 2)
                {
                    diskonOngkir = 10000;
                    diskonOrder = 10000;

                }



                //Console.WriteLine($"Belanja : {order}");
                //Console.WriteLine($"Ongkos Kirim : {ongkirOrder}");
                //Console.WriteLine($"Diskon Ongkir : {diskonOngkir2}");
                //Console.WriteLine($"Diskon Belanja : {diskonOrder2}");
                //Console.WriteLine($"Total Belanja : {order + ongkirOrder - diskonOrder2 - diskonOngkir2}");
            }
            //else if (order >= 100000 && voucherOrder == 1)
            else if (order >= 100000)
            {
                if (voucherOrder == 1)
                {

                    diskonOngkir = 5000;
                    diskonOrder = 5000;
                }
                else if (voucherOrder == 2)
                {
                    diskonOngkir = 10000;
                    diskonOrder = 10000;
                }
                else if (voucherOrder == 3)
                {
                    diskonOngkir = 20000;
                    diskonOrder = 10000;
                }

                //Console.WriteLine($"Belanja : {order}");
                //Console.WriteLine($"Ongkos Kirim : {ongkirOrder}");
                //Console.WriteLine($"Diskon Ongkir : {diskonOngkir3}");
                //Console.WriteLine($"Diskon Belanja : {diskonOrder3}");
                //Console.WriteLine($"Total Belanja : {order + ongkirOrder - diskonOrder3 - diskonOngkir3}");
            }
            //else
            //{
            //diskonOngkir = 0;
            //diskonOrder = 0;
            //Console.WriteLine("Tidak ada diskon");
            //}

            diskonOngkir = ongkirOrder <= diskonOngkir ? ongkirOrder : diskonOngkir;

            double totalBelanja = (order + ongkirOrder) - (diskonOngkir + diskonOrder);
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine($"Belanja : {order}");
            Console.WriteLine($"Ongkos Kirim : {ongkirOrder}");
            Console.WriteLine($"Diskon Ongkir : {diskonOngkir}");
            Console.WriteLine($"Diskon Belanja : {diskonOrder}");
            Console.WriteLine($"Total Belanja : {totalBelanja}");
        }


        static void soalEnam()
        {
            double tunjangan, gapok, banyakBulan;
            string namaSoalEnam;

            Console.Write("Nama : ");
            namaSoalEnam = Console.ReadLine();
            Console.Write("Tunjangan : ");
            tunjangan = double.Parse(Console.ReadLine());
            Console.Write("Gapok : ");
            gapok = double.Parse(Console.ReadLine());
            Console.Write("Banyak Bulan : ");
            banyakBulan = double.Parse(Console.ReadLine());


            if (gapok + tunjangan <= 5000000)
            {
                Console.WriteLine($"Karyawan atas nama {namaSoalEnam} slip gaji sebagai berikut");
                Console.Write($"Pajak : {0.05 * gapok}");
                double pajak = double.Parse(Console.ReadLine());
                Console.Write($"BPJS : {0.03 * gapok}");
                double bpjs = double.Parse(Console.ReadLine());
                Console.Write($"Gaji/bulan : {gapok - pajak - bpjs}");
                double gaji = double.Parse(Console.ReadLine());
                Console.Write($"Total gaji : {gaji * banyakBulan}");
                double totalGaji = double.Parse(Console.ReadLine());
            }
            else if (gapok + tunjangan >= 5000000 && gapok <= 10000000)
            {
                Console.WriteLine("Karyawan atas nama Billy slip gaji sebagai berikut");
                Console.WriteLine($"Pajak : {0.1 * (gapok + tunjangan)}");
                double pajak = double.Parse(Console.ReadLine());
                Console.WriteLine($"BPJS : {0.03 * (gapok + tunjangan)}");
                double bpjs = double.Parse(Console.ReadLine());
                Console.WriteLine($"Gaji/bulan : {gapok + tunjangan - pajak - bpjs}");
                double gaji = double.Parse(Console.ReadLine());
                Console.WriteLine($"Total gaji : {gaji * banyakBulan}");
                double totalGaji = double.Parse(Console.ReadLine());
            }
            else if (gapok + tunjangan > 10000000)
            {
                Console.WriteLine("Karyawan atas nama Billy slip gaji sebagai berikut");
                Console.WriteLine($"Pajak : {0.15 * (gapok + tunjangan)}");
                double pajak = double.Parse(Console.ReadLine());
                Console.WriteLine($"BPJS : {0.03 * (gapok + tunjangan)}");
                double bpjs = double.Parse(Console.ReadLine());
                Console.WriteLine($"Gaji/bulan : {gapok + tunjangan - pajak - bpjs}");
                double gaji = double.Parse(Console.ReadLine());
                Console.WriteLine($"Total gaji : {gaji * banyakBulan}");
                double totalGaji = double.Parse(Console.ReadLine());
            }
            else
            {
                Console.WriteLine("Gaji tidak terdaftar");
            }


            static void soalTiga()
            {

                string promo;
                double belanja, jarak;

                Console.Write("Belanja : ");
                belanja = double.Parse(Console.ReadLine());
                Console.Write("Jarak : ");
                jarak = double.Parse(Console.ReadLine());
                Console.Write("Masukan Promo = ");
                promo = Console.ReadLine();
                double maxDiskon = belanja * 0.4;
                double ongkir = ((jarak - 5) * 1000) + 5000; //per km
                                                             // 5 KM pertama 5000

                if (belanja >= 30000 && promo == "JKTOVO" && maxDiskon <= 30000)
                {
                    Console.WriteLine($"Belanja : {belanja}");
                    //Console.WriteLine($"Diskon : {maxDiskon * belanja});
                    Console.WriteLine($"Diskon 40% : {maxDiskon}");
                    Console.WriteLine($"Ongkir : {ongkir}");
                    Console.WriteLine($"Total Belanja : {belanja + maxDiskon + ongkir}");
                }
            }

        }
    }
}

