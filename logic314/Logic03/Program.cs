﻿using System;

namespace Logic03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //modulusDua();
            //soalDuaL3();
            //soalTigaL3();
            //soalEmpatL3();
            //modulusDua();
            //perulanganWhile();
            //perulanganForInc();
            //perulanganForDec();
            //Break();
            //Continue();
            //soalSatu();
            //SoalDua();
            //soalTiga();
            //soalLima();
            //soalTujuh();
            soalDelapan();

            Console.ReadKey();

        }

        static void soalDelapan()
        {

            double nilaiMTK, nilaiFisika, nilaiKimia;
            Console.Write("Masukkan nilai MTK : ");
            nilaiMTK = double.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Fisika : ");
            nilaiFisika = double.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai Kimia : ");
            nilaiKimia = double.Parse(Console.ReadLine());

            double nilaiRata = (nilaiMTK + nilaiFisika + nilaiKimia) / 3;


            if (nilaiRata >= 75 && nilaiRata < 100)
            {
                Console.WriteLine($"Nilai Rata-Rata : {nilaiRata}");
                Console.WriteLine("Selamat Kamu Berhasil Kamu Hebat");

            }
            if (nilaiRata >= 0 && nilaiRata < 75)
            {
                Console.Write($"Nilai Rata-Rata : {nilaiRata}");
                Console.WriteLine("Maaf Kamu Gagal");
            }
        }

        static void soalTujuh()
        {
            double berat, tinggi;

            Console.Write("Masukkan berat badan anda (kg) : ");
            berat = double.Parse(Console.ReadLine());
            Console.Write("Masukkan tinggi badan anda (cm) : ");
            tinggi = double.Parse(Console.ReadLine());

            double bmi = (berat) / ((tinggi * tinggi)/10000);

            if (bmi >= 18.5 && bmi < 25)
            {
                Console.WriteLine($"Nilai bmi Anda adalah : {bmi}");
                Console.WriteLine("Anda termasuk berbadan langsing");
            }

            else if (bmi >= 25)
            {
                Console.WriteLine($"Nilai bmi Anda adalah : {bmi}");
                Console.WriteLine("Anda termasuk berbadan gemuk");
            }
            else
            {
                Console.WriteLine("Masukkan nilai yang sesuai");
            }


            static void soalLima()
            {

                string nama;
                int tahun;
                Console.Write("Masukkan nama Anda : ");
                nama = Console.ReadLine();
                Console.Write("Tahun berapa Anda lahir? ");
                tahun = int.Parse(Console.ReadLine());


                if (tahun >= 1944 && tahun <= 1964)
                {
                    Console.WriteLine($"{nama}, berdasarkan tahun lahir Anda tergolong Baby Boomer");
                }
                else if (tahun >= 1965 && tahun <= 1979)
                {
                    Console.WriteLine($"{nama}, berdasarkan tahun lahir Anda tergolong Generasi X");
                }
                else if (tahun <= 1980 && tahun <= 1994)
                {
                    Console.WriteLine($"{nama}, berdasarkan tahun lahir Anda tergolong Generasi Y (Millenials");
                }
                else
                {
                    Console.WriteLine($"{nama}, berdasarkan tahun lahir Anda tergolong Generasi Z");
                }
            }

            static void soalTiga()
            {

                string promo;
                double belanja, jarak;

                Console.Write("Belanja : ");
                belanja = double.Parse(Console.ReadLine());
                Console.Write("Jarak : ");
                jarak = double.Parse(Console.ReadLine());
                Console.Write("Masukan Promo = ");
                promo = Console.ReadLine();
                double maxDiskon = belanja * 0.4;
                double ongkir = ((jarak - 5) * 1000) + 5000; //per km
                                                             // 5 KM pertama 5000

                if (belanja >= 30000 && promo == "JKTOVO" && maxDiskon <= 30000)
                {
                    Console.WriteLine($"Belanja : {belanja}");
                    //Console.WriteLine($"Diskon : {maxDiskon * belanja});
                    Console.WriteLine($"Diskon 40% : {maxDiskon}");
                    Console.WriteLine($"Ongkir : {ongkir}");
                    Console.WriteLine($"Total Belanja : {belanja + maxDiskon + ongkir}");
                }
            }

            static void SoalDua()
            {

                Console.WriteLine("--Dapat Poin--");
                //Console.WriteLine("Jika membeli pulsa 10000 maka mendapatkan poin 80");
                //Console.WriteLine("Jika membeli pulsa 20000 maka mendapatkan poin 80");
                Console.Write("Masukkan jumlah pulsa : ");
                int pulsa = int.Parse(Console.ReadLine());
                int poin1 = 80;
                int poin2 = 200;
                int poin3 = 400;
                int poin4 = 800;


                if (pulsa >= 10000 && pulsa < 25000)
                {
                    Console.WriteLine($"Pulsa : {pulsa}");
                    Console.WriteLine($"Poin : {poin1}");
                }
                else if (pulsa >= 25000 && pulsa < 50000)
                {
                    Console.WriteLine($"Pulsa : {pulsa}");
                    Console.WriteLine($"Poin : {poin2}");
                }
                else if (pulsa >= 50000 && pulsa < 100000)
                {
                    Console.WriteLine($"Pulsa : {pulsa}");
                    Console.WriteLine($"Poin : {poin3}");
                }
                else if (pulsa == 100000)
                {
                    Console.WriteLine($"Pulsa : {pulsa}");
                    Console.WriteLine($"Poin : {poin4}");
                }
                else
                {
                    Console.WriteLine("Tidak mendapatkan poin");
                }
            }


            static void SoalSatu()
            {
                Console.WriteLine("---Grade Mahasiswa---");
                Console.Write("Masukkan grade mahasiswa : ");
                int nilai = int.Parse(Console.ReadLine());
                int nilaiMax = 100;

                if (nilai >= 90 && nilai <= nilaiMax)
                {
                    Console.WriteLine("Grade A");
                }
                else if (nilai >= 70 && nilai < 90)
                {
                    Console.WriteLine("Grade B");
                }
                else if (nilai >= 50 && nilai < 70)
                {
                    Console.WriteLine("Grade C");
                }
                else if (nilai < 50 && nilai >= 0)
                {
                    Console.WriteLine("Nilai E");
                }
                else
                {
                    Console.WriteLine("Masukkan nilai yang sesuai");
                }
            }

            static void Continue()
            {
                for (int i = 0; i < 10; i++)
                {
                    if (i == 5)
                    {
                        continue;
                    }
                    Console.WriteLine(i);
                }
            }

            static void Break()
            {
                for (int i = 0; i < 10; i++)
                {
                    if (i == 5)
                    {
                        break;
                    }
                    Console.WriteLine(i);
                }
            }

            static void perulanganForDec()
            {
                for (int i = 10; i > 0; i--)
                {
                    Console.WriteLine(i);
                }
            }

            static void perulanganForInc()
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine(i);
                }
            }

            static void PerulanganDoWhile()
            {
                int a = 0;
                do
                {
                    Console.WriteLine(a);
                    a++;
                }
                while (a < 5);
            }

            static void PerulanganWhile()
            {
                Console.WriteLine("--perulangan While--");
                bool ulangi = true;
                int nilai = 1;

                while (ulangi)
                {
                    Console.WriteLine($"proses ke: {nilai}");
                    nilai++;

                    Console.Write($"Apakah Anda akan mengulangi proses? (y/n)");
                    string input = Console.ReadLine().ToLower();

                    if (input == "y")
                    {
                        ulangi = true;
                    }
                    else if (input == "n")
                    {
                        ulangi = false;
                    }
                    else
                    {
                        nilai = 1;
                        Console.WriteLine("Input yang anda masukkan salah");
                        ulangi = true;
                        //   }
                        // }
                    }

                    static void SoalEmpatL3()
                    {

                        int contohAngka;

                        Console.Write("Masukkan angka : ");
                        contohAngka = int.Parse(Console.ReadLine());


                        if (contohAngka % 2 == 0)
                        {
                            Console.WriteLine($"Angka {contohAngka} adalah genap");
                        }
                        else
                        {
                            Console.WriteLine($"Angka {contohAngka} adalah ganjil");
                        }

                    }

                    static void SoalTigaL3()
                    {
                        Console.WriteLine("--Grade Nilai--");
                        Console.Write("Masukkan nilai : ");
                        int grade = int.Parse(Console.ReadLine());
                        int maxGrade = 100;

                        if (grade >= 80 && grade <= maxGrade)
                        {
                            Console.WriteLine("Kamu mendapatkan grade A");
                        }
                        else if (grade >= 60 && grade <= 80)
                        {
                            Console.WriteLine("Kamu mendapatkan grade B");
                        }
                        else if (grade < 60 && grade > 0)
                        {
                            Console.WriteLine("Kamu mendapatkan grade C");
                        }
                        else
                        {
                            Console.WriteLine("Masukkan grade yang sesuai");
                        }

                    }

                    static void SoalDuaL3()
                    {
                        int puntungRokok;
                        int batangRokok;
                        int sisaRokok;
                        int penghasilan;
                        int harga;

                        harga = 500;

                        Console.WriteLine("Masukkan jumlah puntung rokok : ");
                        puntungRokok = int.Parse(Console.ReadLine());

                        batangRokok = puntungRokok / 8;
                        sisaRokok = puntungRokok % 8;
                        penghasilan = batangRokok * harga;


                        Console.WriteLine($"Batang yang berhasil dirangkai adalah {batangRokok}");
                        Console.WriteLine($"Sisa puntung rokok yang dihasilkan adalah {sisaRokok}");
                        Console.WriteLine($"Pengahasilan adalah Rp{penghasilan}");

                    }

                    static void ModulusDua()
                    {
                        int angkaModulusDua;
                        int pembagiModulusDua;
                        //int hasilmodulus;

                        Console.WriteLine("Masukkan angka pertama : ");
                        angkaModulusDua = int.Parse(Console.ReadLine());
                        Console.WriteLine("Masukkan angka kedua : ");
                        pembagiModulusDua = int.Parse(Console.ReadLine());

                        //hasilmodulus = angkaModulusDua % pembagiModulusDua;

                        if (angkaModulusDua % pembagiModulusDua == 0)
                        {
                            Console.WriteLine($"Angka {angkaModulusDua} % angka {pembagiModulusDua} adalah {angkaModulusDua % pembagiModulusDua }");
                        }
                        else
                        {

                            Console.WriteLine($"Angka {angkaModulusDua} % angka {pembagiModulusDua} bukan 0 melainkan {angkaModulusDua % pembagiModulusDua}");
                        }
                    }
                }
            }
        }
    }
}


