-- Tugas SQL Day 03 Ver 2

create database DB_HR

create table tb_karyawan(
id bigint primary key identity(1,1),
nip varchar(50) not null,
nama_depan varchar(50) not null,
nama_belakang varchar(50) not null,
jenis_kelamin varchar(50) not null,
agama varchar(50) not null,
tempat_lahir varchar(50) not null,
tgl_lahir date,
alamat varchar(100) not null,
pendidikan_terakhir varchar(50) not null,
tgl_masuk date
)


insert into tb_karyawan
(nip, nama_depan, nama_belakang, jenis_kelamin, agama, tempat_lahir, tgl_lahir, alamat, pendidikan_terakhir, tgl_masuk)
values
('001', 'Hamidi', 'Samsudin', 'Pria', 'Islam', 'Sukabumi', '1977-04-21', 'Jl. Sudirman No.12', 'S1 Teknik Mesin', '2015-12-07'),
('002', 'Ghandi', 'Wamida', 'Wanita', 'Islam', 'Palu', '1992-01-12', 'Jl.Rambutan No.22', 'SMA Negeri 02 Palu', '2014-12-01'),
('003', 'Paul', 'Christian', 'Pria', 'Kristen', 'Ambon', '1980-05-27', 'Jl. Veteran No. 4', 'S1 Pendidikan Geografi', '2014-01-12')

create table tb_divisi(
id bigint primary key identity(1,1),
kd_divisi varchar(50) not null,
nama_divisi varchar(50) not null
)

insert into tb_divisi (kd_divisi, nama_divisi) values
('GD', 'Gudang'),
('HRD', 'HRD'),
('KU', 'Keuangan'),
('UM', 'Umum')

create table tb_jabatan(
id bigint primary key identity(1,1),
kd_jabatan varchar(50) not null,
nama_jabatan varchar(50) not null,
gaji_pokok numeric,
tunjangan_jabatan numeric
)

insert into tb_jabatan (kd_jabatan, nama_jabatan, gaji_pokok, tunjangan_jabatan) values
('MGR','Manager', 5500000, 1500000),
('OB', 'Office Boy', 1900000, 200000),
('ST', 'Staff', 3000000, 750000),
('WMGR', 'Wakil Manager', 4000000, 1200000)

create table tb_pekerjaan(
id bigint primary key identity(1,1),
nip varchar(50) not null,
kode_jabatan varchar(50) not null,
kode_divisi varchar(50) not null,
tunjangan_kinerja numeric,
kota_penempatan varchar(50)
)

insert into tb_pekerjaan (nip, kode_jabatan, kode_divisi, tunjangan_kinerja, kota_penempatan) values
('001', 'ST', 'KU', 750000, 'Cianjur'),
('002', 'OB', 'UM', 350000, 'Sukabumi'),
('003', 'MGR', 'HRD', 1500000, 'Sukabumi')

--1. Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji , yang gaji + tunjangan kinerja dibawah 5juta									

select * from tb_karyawan
select * from tb_divisi
select * from tb_pekerjaan
select * from tb_jabatan

select concat(nama_depan, ' ', nama_belakang) as [Nama Lengkap], 
tb_jabatan.nama_jabatan, tb_jabatan.tunjangan_jabatan + tb_jabatan.gaji_pokok [Total_Gaji]
from tb_karyawan
join tb_pekerjaan
on tb_karyawan.nip = tb_pekerjaan.nip
join tb_jabatan
on tb_jabatan.kd_jabatan = tb_pekerjaan.kode_jabatan
where tb_jabatan.tunjangan_jabatan + tb_jabatan.gaji_pokok < 5000000

--2. Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi

select * from tb_karyawan
select * from tb_divisi
select * from tb_pekerjaan
select * from tb_jabatan

select concat(kr.nama_depan, ' ', kr.nama_belakang) [Nama Lengkap], div.nama_divisi, jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja [Total_Gaji],
(jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja)*0.05 [Pajak], (jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja) - 
((jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja)*0.05) [Gaji_Bersih]
from tb_karyawan as kr
join tb_pekerjaan as pkr
on kr.nip = pkr.nip
join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
join tb_jabatan as jbt
on jbt.kd_jabatan = pkr.kode_jabatan
where
kr.jenis_kelamin = 'Pria' AND pkr.kota_penempatan != 'SUKABUMI'

--3. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja) * 7													

select * from tb_karyawan
select * from tb_divisi
select * from tb_pekerjaan
select * from tb_jabatan

select kr.nip, CONCAT(kr.nama_depan, ' ', kr.nama_belakang) [Nama_Lengkap], 
div.nama_divisi, 0.25 * (jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja) * 7 [Bonus]
from tb_karyawan as kr
join tb_pekerjaan as pkr
on kr.nip = pkr.nip
join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
join tb_jabatan as jbt
on jbt.kd_jabatan = pkr.kode_jabatan 

--4. Tampilkan nama lengkap, total gaji, infak(5%*total gaji) yang mempunyai jabatan MGR	

select kr.nip, CONCAT(kr.nama_depan, ' ', kr.nama_belakang) [Nama_Lengkap], jbt.nama_jabatan, div.nama_divisi,
jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja [Total_Gaji],
(jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja) * 0.05 [Infaq]
from tb_karyawan as kr
join tb_pekerjaan as pkr
on kr.nip = pkr.nip
join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
join tb_jabatan as jbt
on jbt.kd_jabatan = pkr.kode_jabatan
where jbt.kd_jabatan = 'MGR'


--5. Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1

select * from tb_karyawan
select * from tb_divisi
select * from tb_pekerjaan
select * from tb_jabatan

select kr.nip, CONCAT(kr.nama_depan, ' ', kr.nama_belakang) [Nama_Lengkap], jbt.nama_jabatan, CAST(2000000 as int) [Tunjangan_Pendidikan], 
jbt.gaji_pokok + jbt.tunjangan_jabatan + CAST(2000000 as int)
from tb_karyawan as kr
join tb_pekerjaan as pkr
on kr.nip = pkr.nip
join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
join tb_jabatan as jbt
on jbt.kd_jabatan = pkr.kode_jabatan
where kr.pendidikan_terakhir like '%S1%'
order by kr.nip

--6. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus					
	--Keterangan :	MGR = (bonus=25% dari total gaji*7) ,				
	--ST = (bonus=25% dari total gaji*5) ,				
	--other = (bonus=25% dari total gaji*2)

select * from tb_karyawan
select * from tb_divisi
select * from tb_pekerjaan
select * from tb_jabatan

select kr.nip, CONCAT(kr.nama_depan, ' ', kr.nama_belakang) [Nama_Lengkap], jbt.nama_jabatan, div.nama_divisi,
case when jbt.kd_jabatan = 'MGR' then  ((jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja) * 7 * 0.25)
	when jbt.kd_jabatan ='ST' then ((jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja) * 5 * 0.25)
	else ((jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja) * 2 * 0.25)
	end as [Bonus]
from tb_karyawan as kr
join tb_pekerjaan as pkr
on kr.nip = pkr.nip
join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
join tb_jabatan as jbt
on jbt.kd_jabatan = pkr.kode_jabatan


--7. Buatlah kolom nip pada table karyawan sebagai kolom unique	

alter table tb_karyawan add constraint unique_nip unique (nip)

--8. Buatlah kolom nip pada table karyawan sebagai index		

create index index_nip on tb_karyawan(nip)


--9. Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W	

select * from tb_karyawan
select * from tb_divisi
select * from tb_pekerjaan
select * from tb_jabatan


select 
case
	when nama_belakang like 'w%' then  nama_depan + ' ' + Upper(nama_belakang)
	else nama_depan + ' ' + nama_belakang
end as Nama_Lengkap
from tb_karyawan

--10.  Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas sama dengan  8 tahun													
--Tampilkan nip, nama lengkap, jabatan, nama divisi, total gaji , bonus, lama bekerja													
--(total gaji = Gaji_pokok+Tunjangan_jabatan+Tunjangan_kinerja )

select kr.nip, CONCAT(kr.nama_depan, ' ', kr.nama_belakang) [Nama_Lengkap], jbt.nama_jabatan, div.nama_divisi,
jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja [Total_Gaji],
(jbt.gaji_pokok + jbt.tunjangan_jabatan + pkr.tunjangan_kinerja) * 0.1 [Bonus],
datediff(year, kr.tgl_masuk, getdate()) [Lama_Bekerja]
from tb_karyawan as kr
join tb_pekerjaan as pkr
on kr.nip = pkr.nip
join tb_divisi as div
on div.kd_divisi = pkr.kode_divisi
join tb_jabatan as jbt
on jbt.kd_jabatan = pkr.kode_jabatan
where
datediff(year, kr.tgl_masuk, getdate()) >= 8

								
