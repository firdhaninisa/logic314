--Simulasi SQL

create database DB_PTXA

create table Biodata(
id bigint primary key identity(1,1),
first_name varchar(20),
last_name varchar(30),
dob datetime,
pob varchar(50),
address varchar(255),
gender varchar(1)
)

insert into Biodata (first_name, last_name, dob, pob, address, gender) values
('soraya', 'rahayu','1990-12-22', 'Bali', 'Jl. Raya Kuta, Bali', 'P'),
('hanum', 'danuary','1990-01-02', 'Bandung', 'Jl. Berkah Ramadhan, Bandung', 'P'),
('melati', 'marcelia','1991-03-03', 'Jakarta', 'Jl. Mawar 3, Brebes', 'P'),
('farhan', 'Djokrowidodo','1989-10-11', 'Jakarta', 'Jl. Bahari Raya, Solo', 'L')

