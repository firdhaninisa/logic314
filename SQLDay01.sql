--CREATE DATABASE
create database db_kampus
create database db_kampus2

--create table
create table mahasiswa (
id bigint primary key identity(1,1),
name varchar(50) not null,
address varchar(50) not null,
email varchar(255) null
)

--create table 2
create table mahasiswa2 (
id bigint primary key identity(1,1),
name varchar(50) not null,
address varchar(50) not null,
email varchar(255) null
)

--create view
create view vwMahasiswa
as
select id, name, address, email from mahasiswa

--select view
select * from mahasiswa


--alter add column
alter table mahasiswa add description varchar(255)

--alter drop column
alter table mahasiswa drop column [description]

--table alter column
alter table mahasiswa alter column email varchar(100)

--alter view
--alter view [NamaviewLama] to [NamaViewBaru]

--------------------------------------------------------------------------

--drop database

drop database db_kampus2

--drop table

drop table mahasiswa2

--drop view
drop view [NamaView]

--------------------------------------------------------------- -----------

--DML--

--insert
insert into mahasiswa (name, address, email) values ('Marchel','Medan','marchelajadeh@gmail.com')

insert into mahasiswa (name, address, email) values ('Marchel','Medan2','marchelajadeh@gmail.com')

insert into mahasiswa (name, address, email) values ('Toni', 'Garut', 'toni@gmail.com')

insert into mahasiswa (name, address, email) values ('Isni', 'Cimahi', 'isni@gmail.com')

--select
select id, name, address, email from mahasiswa
select * from mahasiswa

--update data
update mahasiswa set name='Isni Dwitiniardi',address='Sumedang' where name='isni'

--delete data
delete from mahasiswa where name='Toni'

---------------------------------------------------------------------

create table biodata(
id bigint primary key identity (1,1),
dob datetime not null,
kota varchar (100)
)

alter table biodata add mahasiswa_id bigint
alter table biodata alter column mahasiswa_id bigint not null

--join table
select *
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.name = 'toni'

--join table
select mhs.id as ID, mhs.name, bio.kota, bio.dob,
month(bio.dob)BulanLahir, year(bio.dob)TahunLahir
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.name = 'toni' or bio.kota='jakarta'

insert into biodata (dob, kota, mahasiswa_id) values ('2000-03-15', 'Jakarta', 1), ('1998-08-05', 'Jakarta', 4), ('2000-06-26', 'Jakarta', 3)

update biodata set kota='Dan Mogot' where mahasiswa_id= 1
update biodata set kota='Kalibata' where mahasiswa_id= 4
update biodata set kota='Gandaria' where mahasiswa_id= 3

select *from biodata

alter table biodata alter column dob date not null

--order by
select *
from mahasiswa mhs
join biodata bio on mhs.id = bio.mahasiswa_id
order by mhs.id asc, mhs.name desc

--select top
select top 2 * from mahasiswa order by name desc

--between
select * from mahasiswa where id between 1 and 3
select * from mahasiswa where id >=1 and id <=3

--like

select * from mahasiswa where name like 'm%'
select * from mahasiswa where name like '%i'
select * from mahasiswa where name like '%dwi%'
select * from mahasiswa where name like '_o%'
select * from mahasiswa where name like '__n%'
select * from mahasiswa where name like 't_%'
select * from mahasiswa where name like 't_____%'
select * from mahasiswa where name like 'i%i'

-- group by
select sum(id),name from mahasiswa group by name
select count(id),name from mahasiswa group by name

--having
select count(id),name
from mahasiswa group by name
having count(id) > 1

--distinct
select distinct name from mahasiswa

--subString
select substring('SQL Tutorial', 1, 3) as Judul

--CharIndex
select charindex('t', 'Customer') as Judul

--DataLength
select datalength('akumau.istirahat')

-----------------------------------------------------------------

alter table mahasiswa add panjang smallint

update mahasiswa set panjang = 48 where id=1
update mahasiswa set panjang = 86 where name='Toni'
update mahasiswa set panjang = 177 where name='Isni Dwitiniardi'
update mahasiswa set panjang = 50 where id=5

select * from mahasiswa order by panjang asc

--case when
select id, name, address, panjang,
	case when panjang < 50 then 'Pendek'
	when panjang <= 100 then 'Sedang'
	else 'Tinggi'
	end as TinggiBadan
from mahasiswa

--concat
select concat('SQL ', ' is ', ' fun!')
select 'SQL ' + 'is ' + 'fun!'

-----------------------------------------------------------------

--Tabel Penjualan

create table TabelPenjualan (
id bigint primary key identity(1,1),
nama varchar (50) not null,
harga int not null
)

insert into TabelPenjualan (nama, harga) values 
('Indomie', 1500),
('Close-Up', 3500),
('Pepsoden', 3000),
('Brush Formula', 2500),
('Roti Manis', 1000),
('Gula', 3500),
('Sarden', 4500),
('Rokok Sampurna', 11000),
('Rokok 234', 11000)

select nama, harga, harga * 100 as [harga * 100] from TabelPenjualan

-- Memunculkan struktur data

sp_column 'tblPengarang'