--KISI-KISI

create database DB_KISIKISI

create table assignment(
id int primary key identity (1,1),
name varchar(50) not null,
marks varchar(50) null,
grade int not null
)

insert into assignment(name,marks) values
('Isni', 85),
('Laudry', 75),
('Bambang', 40),
('Anwar', 91),
('Alwi', 70),
('Fulan', 50)

select * from assignment

select name, marks, 
case
when marks > 90 then 'A+'
when marks > 70 then 'A-'
when marks > 60 then 'B'
when marks > 40 then 'C'
else 'FAIL'
end
as [grade]
from assignment