--Tugas SQLDay01

create table tblPengarang(
id bigint primary key identity(1,1),
kd_Pengarang varchar(7) not null,
Nama varchar (30) not null,
Alamat varchar (30) not null,
Kota varchar (80) not null,
Kelamin varchar(1) not null
)

select * from tblPengarang

insert into tblPengarang (kd_Pengarang, Nama, Alamat, Kota, Kelamin) values
('P0001', 'Ashadi', 'Jl. Beo 25', 'Yogya', 'P'),
('P0002', 'Rian', 'Jl. Solo 123', 'Yogya', 'P'),
('P0003', 'Suwadi', 'Jl. Semangka 13', 'Bandung', 'P'),
('P0004', 'Siti', 'Jl. Durian 15', 'Solo', 'W'),
('P0005', 'Amir', 'Jl. Gajah 33', 'Kudus', 'P'),
('P0006', 'Suparman', 'Jl. Harimau 25', 'Jakarta', 'P'),
('P0007', 'Jaja', 'Jl. Singa 7', 'Bandung', 'P'),
('P0008', 'Saman', 'Jl. Naga 12', 'Yogya', 'P'),
('P0009', 'Anwar', 'Jl. Tidar 6A', 'Magelang', 'P'),
('P0010', 'Fatmawati', 'Jl. Renjana 4', 'Bogor', 'W')

create table tblGaji(
ID int primary key identity(1,1),
Kd_Pengarang varchar (7) not null,
Nama varchar (30) not null,
Gaji decimal (18,4) not null
)

insert into tblGaji (Kd_Pengarang, Nama, Gaji) values
('P0002', 'Rian', 600000),
('P0005', 'Amir', 700000),
('P0004', 'Siti', 500000),
('P0003', 'Suwadi', 1000000),
('P0010', 'Fatmawati', 600000)

select * from tblGaji

--1. Hitung dan tampilkan jumlah pengarang dari tabel tbl Pengarang

select count(id) as jlm_Pengarang, nama from tblPengarang group by nama
select count(id) as total_Pengarang from tblPengarang

--2. Hitunglah beberapa jumlah pengarang wanita dan pria

select Kelamin, count(kelamin) as jml_klm from tblPengarang group by Kelamin

--3. Tampilkan record kota dan jumlah kotanya dari tblPengarang

select kota, count(kota) as jml_kota from tblPengarang group by kota

--4. Tampilkan record kota diatas 1 kota dari tabel tblPengarang

select top 1 kota from tblPengarang
select kota, count(kota) as jml_kota from tblPengarang group by kota having count(kota) > 1
select kota, count(kota) as jml_kota where count(kota) > 1 from tblPengarang group by kota

--5. Tampilkan kd_pengarang yang terbesar dan terkecil dari tabel pengarang
select
(select top 1 kd_pengarang from tblPengarang order by kd_pengarang desc) as kode_tertinggi,
(select top 1 kd_pengarang from tblPengarang order by kd_Pengarang asc) as kode_terendah

select kd_pengarang from tblPengarang order by kd_pengarang desc
select max(kd_pengarang) as kode_tertinggi, min(kd_pengarang) as kode_terendah from tblPengarang

--6. Tampilkan gaji tertinggi dan terendah

select max(gaji) as gaji_tertinggi from tblGaji
select min(gaji) as gaji_terendah from tblGaji

select max(gaji) as gaji_tertinggi, min(gaji) as gaji_terendah from tblGaji

--7. Tampilkan gaji di atas 600.000
select gaji
from tblGaji group by gaji
having gaji > 600000

--8. Tampilkan jumlah gaji
select sum(gaji) as jumlah_gaji from tblGaji

--9. Tampilkan jumlah gaji berdasarkan kota

select pengarang.Kota, sum(gaji) as jumlahGaji
from tblPengarang as pengarang
join tblGaji as gaji
on pengarang.kd_Pengarang = gaji.Kd_Pengarang
group by pengarang.Kota

select tblPengarang.Kota, tblGaji.Gaji, tblPengarang.Nama
from tblPengarang
INNER JOIN tblGaji ON tblGaji.Kd_Pengarang = tblPengarang.kd_Pengarang;

--10. Tampilkan seluruh record pengarang antara P0003 -  P0006 dari tabel pengarang

select id, kd_pengarang, nama from tblPengarang
select Kd_Pengarang, nama from tblPengarang  where id between 6 AND 9

select * from tblPengarang where id between 6 AND 9

--11. Tampilkan seluruh data Yogya, Solo, dan Magelang dari tabel pengarang

select id, kd_Pengarang, Nama, Alamat, Kota, Kelamin from tblPengarang where Kota='Yogya' OR Kota='Solo' OR Kota='Magelang'
select * from tblPengarang where Kota='Yogya' OR Kota='Solo' OR Kota='Magelang'
select * from tblPengarang where Kota in ('Yogya', 'Solo', 'Magelang')

--12. Tampilkan seluruh data yang bukan Yogya dari tabel pengarang

select id, kd_Pengarang, Nama, Alamat, Kota, Kelamin from tblPengarang where Kota != 'Yogya'
select * from tblPengarang where Kota != 'Yogya'

select * from tblPengarang where not Kota = 'Yogya'

--13. Tampilkan seluruh data pengarang yang nama (terpisah)
--a. dimulai dengan huruf A

select id, kd_Pengarang, Nama, Alamat, Kota, Kelamin from tblPengarang where nama like 'a%'
select * from tblPengarang where nama like 'a%'

--b. huruf berakhiran i

select id, kd_Pengarang, Nama, Alamat, Kota, Kelamin from tblPengarang where nama like '%i'
select * from tblPengarang where nama like '%i'
select * from tblPengarang where kd_Pengarang NOT LIKE '%4' AND Nama LIKE '%i'

--c. huruf ketiga a

select * from tblPengarang where nama like '__a%'

--d. tidak berakhiran n

select id, kd_Pengarang, Nama, Alamat, Kota, Kelamin from tblPengarang where nama NOT like '%n'

--14. Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama

select tblPengarang.kd_Pengarang, tblPengarang.id, tblPengarang.Alamat, tblPengarang.Kelamin, tblPengarang.Kota, tblGaji.Gaji
from tblGaji
LEFT JOIN tblPengarang
ON tblPengarang.kd_Pengarang = tblGaji.Kd_Pengarang;

select * from tblPengarang
join tblGaji
on tblPengarang.kd_Pengarang = tblGaji.Kd_Pengarang

--15. Tampilkan kota yang memiliki gaji di bawah 1000000

select tblPengarang.Kota, tblGaji.Gaji
from tblPengarang
JOIN tblGaji
ON tblPengarang.kd_Pengarang = tblGaji.Kd_Pengarang
WHERE
Gaji > 0 AND Gaji < 1000000;

--16. Ubah panjang dari tipe kelamin menjadi 10

alter table tblPengarang alter column kelamin varchar(10)

--17. Tambahkan kolom [gelar] dengan tipe varchar (12) pada tabel tblPengarang

alter table tblPengarang add gelar varchar(12)
select * from tblPengarang

--18. Ubah alamat dan kota dari Rian di tblPengarang menjadi Jl.Cendrawasih 65 dan Pekanbaru

update tblPengarang set Alamat='Jl.Cendrawasih 65', Kota='Pekanbaru' where Nama='Rian'
select * from tblPengarang

--19. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota, Gaji dengan vwPengarang

create view vwPengarang
as
select pengarang.kd_Pengarang, pengarang.Nama, pengarang.Kota, gaji.Gaji
from tblPengarang as pengarang join  tblGaji as gaji
on pengarang.Kd_Pengarang = gaji.Kd_Pengarang

select * from tblPengarang
select * from tblGaji

