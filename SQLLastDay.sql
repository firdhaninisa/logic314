select * from assignment

insert into assignment(name, marks) values
('Data', 100)

delete from assignment where id=7

--tabel employee dan department

create table employee(
id int primary key identity (1,1),
name varchar(50) not null
)

insert into employee(name) values
('Isni'),
('Laudry'),
('Firdha'),
('Asti')

create table department(
id int primary key identity(1,1),
name varchar(50) not null,
salary decimal(18,2) not null
)

alter table department add employee_id int not null

alter table department drop column employee_id

alter table employee add department_id int

alter table department drop column salary

alter table employee add salary decimal(18,2)

insert into department(name) values
('HRD'),
('Accounting'),
('IT'),
('Finance')

update employee set department_id = 1, salary = 100000 where id=1
update employee set department_id = 1, salary = 50000 where id=2
update employee set department_id = 3, salary = 200000 where id=3
update employee set department_id = 4, salary = 250000 where id=4

select top 1 dep.name [nama_department], avg(emp.salary)
from employee as emp
join department as dep
on emp.department_id = dep.id
group by dep.name
order by avg(emp.salary) desc